2019
# Real-time Adaptive Motion Planning(RAMP) for Manipulators

This repository implements the algorithm of Real-time Adaptive Motion Planning(RAMP) for manipulators.
Dependency: ROS and MoveIt!

## ramp_core
* initialization (with ramp_setting ramp_goal)
* planning cycle
* control cycle
* sensing cycle
* rendering (graphic display with ramp_rendering)

## ramp_setting
* specify kinematic chain (through MoveIt!)
* specify kinematic constraints
* collsion checking with FCL may also be done here

## ramp_goal
* specify goal pose for eef and other task constraints
* specify evaluation function

## ramp_rendering
* maintain simulation scene

## ramp_trajectory
* start/end arm configuration
* knot configurations
* duration 
* trajectory generation from paths with velocity/acceleration limits
* trajectory evaluation 
* feasibility checking

## ramp_operators
* insert/delete/change/swap/crossover/stop

## ramp_population 
* diversity creation
* diversity presvation 

## ramp_sensing 
* sense obstacle and predict their motion (with ramp_obstacle)

## ramp_obstacle
* obstacle pose and velocity
* obstacle representation for collision checking

## ramp_sampling
* sample random joint configurations
* sample joint configurations given eef pose
* data structures for constrained pose (under soft constraints)

## ramp_util
* utility function for converting orientation representations


