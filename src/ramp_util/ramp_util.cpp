/*
 * ramp_util.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_util/ramp_util.h"

void rampUtil::convertXYZRPYtoAffine3d(const std::vector<double> &pose_vec, Eigen::Affine3d &pose_tf)
{
	Eigen::Matrix3d rot;
	rot = Eigen::AngleAxisd(pose_vec[3], Eigen::Vector3d::UnitX())
		 *Eigen::AngleAxisd(pose_vec[4], Eigen::Vector3d::UnitY())
		 *Eigen::AngleAxisd(pose_vec[5], Eigen::Vector3d::UnitZ());
	pose_tf = rot;
	pose_tf.translation() = Eigen::Vector3d(pose_vec[0],pose_vec[1],pose_vec[2]);
}

void rampUtil::convertAffine3dtoQuaternion(const Eigen::Affine3d &pose_tf, geometry_msgs::Pose &pose_quat)
{
	pose_quat.position.x = pose_tf.translation()[0];
	pose_quat.position.y = pose_tf.translation()[1];
	pose_quat.position.z = pose_tf.translation()[2];
	Eigen::Quaterniond q(pose_tf.rotation());
	pose_quat.orientation.x = q.x();
	pose_quat.orientation.y = q.y();
	pose_quat.orientation.z = q.z();
	pose_quat.orientation.w = q.w();
}

void rampUtil::convertXYZRPYtoQuaternion(const std::vector<double> &pose_vec, geometry_msgs::Pose &pose_quat)
{
	Eigen::Affine3d tmp;
	convertXYZRPYtoAffine3d(pose_vec, tmp);
	convertAffine3dtoQuaternion(tmp, pose_quat);
}

void rampUtil::printGeoMsgPose(geometry_msgs::Pose &p)
{
	ROS_INFO("Pose is [%f, %f, %f, %f, %f, %f, %f]",p.position.x,
													p.position.y,
													p.position.z,
													p.orientation.x,
													p.orientation.y,
													p.orientation.z,
													p.orientation.w);
}

void rampUtil::printArmCfg(armCfg &jv)
{
	if (jv.size()==6)
		ROS_INFO("jv is %f, %f, %f, %f, %f, %f",jv[0],
												jv[1],
												jv[2],
												jv[3],
												jv[4],
												jv[5]);
	if (jv.size()==7)
		ROS_INFO("jv is %f, %f, %f, %f, %f, %f, %f",jv[0],
													jv[1],
													jv[2],
													jv[3],
													jv[4],
													jv[5],
													jv[6]);
}






 
