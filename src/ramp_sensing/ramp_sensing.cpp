/*
 * ramp_sensing.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_sensing/ramp_sensing.h"

rampSensing::rampSensing(ros::NodeHandle &nh)
{
	sensing_topic = "/gazebo/model_states";
	sub = nh.subscribe(sensing_topic, 1, &rampSensing::callback, this);
	num_of_sensing_cyc = 0;
}

void rampSensing::addObstacle(rampObstacle &rob)
{
	obstacles.push_back(rob);
}

void rampSensing::removeObstacleByID(const std::string &id)
{
	int erase_ind = -1;
	for (int i=0; i<obstacles.size(); ++i)
	{
		if (obstacles.at(i).getID() == id)
		{
			obstacles.at(i).clear();
			erase_ind = i;
			ROS_INFO("Removed obstacle with ID %s", id.c_str());
		}
	}
	if (erase_ind > 0)
		obstacles.erase(obstacles.begin() + erase_ind);
}

void rampSensing::callback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
	/*
		for drone in hector_quadrotor, msg->pose is a vector and 
		has 2 elements, 1st is for "ground plane", 2nd is for "quadrotor"
	*/ 
	ROS_INFO("Drone position [%f %f %f] orientation [%f %f %f %f]", msg->pose[1].position.x,
																	msg->pose[1].position.y,
																	msg->pose[1].position.z,
																	msg->pose[1].orientation.x,
																	msg->pose[1].orientation.y,
																	msg->pose[1].orientation.z,
																	msg->pose[1].orientation.w);
	ROS_INFO("Drone twist linear [%f %f %f] angular [%f %f %f]",msg->twist[1].linear.x,
																msg->twist[1].linear.y,
																msg->twist[1].linear.z,
																msg->twist[1].angular.x,
																msg->twist[1].angular.y,
																msg->twist[1].angular.z);
	update();
	++num_of_sensing_cyc;
}

void rampSensing::update()
{	
	geometry_msgs::Pose pose;
	Eigen::VectorXf vel;
	for (int i=0; i<obstacles.size(); ++i)
	{
		obstacles.at(i).setNewPose(pose);
		obstacles.at(i).setNewVelocity(vel);
	}
}

