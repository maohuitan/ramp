/*
 * ramp_obstacle.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_obstacle/ramp_obstacle.h"

rampObstacle::rampObstacle( std::string &obs_type, std::string &obs_id,
							const geometry_msgs::Pose &obs_pose, const Eigen::Vector3f &dimension,
							boost::shared_ptr<ros::ServiceClient> &psdc_ptr)
: type(obs_type), id(obs_id), pose(obs_pose), dimens(dimension)
{
	planning_scene_diff_client_ptr = psdc_ptr;
	velocity.resize(6);
	velocity << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
	addToPlanningScene();
}

void rampObstacle::setNewPose(const geometry_msgs::Pose &obs_pose)
{
	pose = obs_pose;
	/*
		if object operation is MOVE, primitives must be left empty
		otherwise move_group would crash
	*/
	attached_object.object.primitives.clear();
	// attached_object.object.primitives.push_back(primitive);
	attached_object.object.primitive_poses.clear();
	attached_object.object.primitive_poses.push_back(pose);
	attached_object.object.operation = attached_object.object.MOVE;

	publishObstacleDiff();
}

void rampObstacle::setNewVelocity(const Eigen::VectorXf &vel)
{
	velocity = vel;	
}

std::string rampObstacle::getID()
{
	return id;	
}

void rampObstacle::clear()
{
	attached_object.object.operation = attached_object.object.REMOVE;
	publishObstacleDiff();
}

void rampObstacle::addToPlanningScene()
{
	attached_object.link_name = "base_link";
	attached_object.object.header.frame_id = "base_link";
	attached_object.object.id = id;

	if (type=="box")
	{
		primitive.type = primitive.BOX;  
		primitive.dimensions.resize(3);
		primitive.dimensions[0] = dimens[0];
		primitive.dimensions[1] = dimens[1];
		primitive.dimensions[2] = dimens[2];  
	}

	if (type=="sphere")
	{
		primitive.type = primitive.SPHERE;  
		primitive.dimensions.resize(1);
		primitive.dimensions[0] = dimens[0];  
	}

	attached_object.object.primitives.push_back(primitive);
	attached_object.object.primitive_poses.push_back(pose);
	attached_object.object.operation = attached_object.object.ADD;

	publishObstacleDiff();
}

void rampObstacle::publishObstacleDiff()
{
	moveit_msgs::PlanningScene planning_scene;
	planning_scene.world.collision_objects.push_back(attached_object.object);
	planning_scene.is_diff = true;

	planning_scene_diff_client_ptr->waitForExistence();
	moveit_msgs::ApplyPlanningScene srv;
	srv.request.scene = planning_scene;
	planning_scene_diff_client_ptr->call(srv);
}

rampObstacle::~rampObstacle()
{
	
}



