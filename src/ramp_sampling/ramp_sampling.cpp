/*
 * ramp_sampling.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_sampling/ramp_sampling.h"

constrainedPose::constrainedPose(const std::vector<double> &base_pose_vec, const std::string axis, const double tolerance)
:m_base_pose_vec(base_pose_vec), m_axis(axis), m_tolerance(tolerance)
{

}

void constrainedPose::getOneConstrainedPose(geometry_msgs::Pose &cstr_pose)
{
	// tf_from_goal_to_world_under_soft_cstr = tf_from_base_goal_to_world
	//										 * tf_from_task_frame_to_base_goal;
	
	/* 1-constrained rotation matrix (tf_from_task_frame_to_base_goal) */
	// a random number between -1 to 1
	float prop = -1.0 + 2.0*((float)rand())/RAND_MAX;
	prop = 1.0;

	std::vector<double> task_frame_vec(6,0.0);
	if (m_axis == "X")
		task_frame_vec[3] += prop*m_tolerance;
	if (m_axis == "Y")
		task_frame_vec[4] += prop*m_tolerance;
	if (m_axis == "Z")
		task_frame_vec[5] += prop*m_tolerance;
	if (m_axis == "XY")
	{
		task_frame_vec[3] += prop*m_tolerance;
		task_frame_vec[4] += prop*m_tolerance;
	}

	ROS_INFO("Randomly generated constrained task frame XYZRPY is [%f, %f, %f, %f, %f, %f]",  task_frame_vec[0], task_frame_vec[1], task_frame_vec[2],
																	task_frame_vec[3], task_frame_vec[4], task_frame_vec[5]);
	Eigen::Affine3d task_frame_tf;
	rampUtil::convertXYZRPYtoAffine3d(task_frame_vec, task_frame_tf);

	/* 2-base pose tf (tf_from_base_goal_to_world) */
	Eigen::Affine3d base_pose_tf;
	Eigen::Matrix3d base_pose_rot;
	base_pose_rot =  Eigen::AngleAxisd(m_base_pose_vec[3], Eigen::Vector3d::UnitX())
					*Eigen::AngleAxisd(m_base_pose_vec[4], Eigen::Vector3d::UnitY())
					*Eigen::AngleAxisd(m_base_pose_vec[5], Eigen::Vector3d::UnitZ());
	base_pose_tf = base_pose_rot;
	base_pose_tf.translation() = Eigen::Vector3d(m_base_pose_vec[0],m_base_pose_vec[1],m_base_pose_vec[2]);
	
	/* 3-new constrained pose (tf_from_goal_to_world_under_soft_cstr)*/
	Eigen::Affine3d cstr_pose_tf = base_pose_tf*task_frame_tf;

	// convert eigen::affine3d to geometry_msgs::Pose
	rampUtil::convertAffine3dtoQuaternion(cstr_pose_tf, cstr_pose);
}

rampSampling::rampSampling(boost::shared_ptr<rampSetting> &rs_ptr)
{
	ramp_setting_ptr = rs_ptr;
}

void rampSampling::sampleRandomJointConfiguration(armCfg &jnt_cfg)
{
	armCfg cur_cfg;
	ramp_setting_ptr->getCurrentJointConfiguration(cur_cfg);
	ramp_setting_ptr->setRandomJointConfiguration();
	ramp_setting_ptr->getCurrentJointConfiguration(jnt_cfg);
	ramp_setting_ptr->setJointConfiguration(cur_cfg);
}

void rampSampling::sampleJntCfgGivenEefPose(armCfg &jnt_cfg,const geometry_msgs::Pose &eef_pose)
{
	std::vector<armCfg> ik_jvs;
	if (ramp_setting_ptr->getAllIKJointConfigurations(ik_jvs, eef_pose))
	{
		ROS_INFO("Sampling Joint Configuration given EEF Pose Successful.");
		// randomly select one when multiple IK solutions exist
		int index = rand() % ik_jvs.size();
		jnt_cfg = ik_jvs.at(index);
	}
	else
		ROS_WARN("Sampling Joint Configuration given EEF Pose Failed");
}


 
