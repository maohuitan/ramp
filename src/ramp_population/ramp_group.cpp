# include "ramp_population/ramp_group.h"


void rampTrajectoryGroup::updateStartingState(
					      const robot_state::RobotStatePtr sample_state_ptr){					      


  rampTrajectorySet::iterator it_container = trajectoryContainer_.begin();
  
  // loop over each path 
  for(int ii = 0; ii<trajectoryContainer_.size(); ii++){
    //  update the starting state
    (*it_container)->updateStartingState(sample_state_ptr);
    it_container++;
  }
  
}


bool rampTrajectoryGroup::updateBestInfeasible(){
  const rampTrajectorySet::index<tag_composite>::type & 
    composite_index = trajectoryContainer_.get<tag_composite>();
    
  if(!bestInfeasibleTrajectoryPtr_){
    // In case that the pointer to the best path is NULL:
    bestInfeasibleTrajectoryPtr_ = *composite_index.begin();
    return true;  
  }
  // If it is evaluated already
  if (bestInfeasibleTrajectoryPtr_ == *composite_index.begin()){
    // If it is not changed
    return false;
  }else{
    // Yes, it is changed
    bestInfeasibleTrajectoryPtr_ = *composite_index.begin();
    return true;
  }
    
}

bool rampTrajectoryGroup::updateBestFeasible(){
  const rampTrajectorySet::index<tag_composite>::type & 
    composite_index = trajectoryContainer_.get<tag_composite>();
  
  rampTrajectorySet::index<tag_composite>::type::iterator 
    it_composite_index = composite_index.begin();
  
  int amount_infeasibility = getInfeasibleTrajectoryNumber();
  
  std::advance(it_composite_index, amount_infeasibility);
  
  if(!bestFeasibleTrajectoryPtr_){
    // In case that the pointer to the best trajectory is NULL:
    bestFeasibleTrajectoryPtr_ = *it_composite_index;
    return true;  
  }
  // If it is evaluated already
  if (bestFeasibleTrajectoryPtr_ == *it_composite_index){
    // If it is not changed
    return false;
  }else{
    // Yes, it is changed
    bestFeasibleTrajectoryPtr_ == *it_composite_index;
    return true;
  }
}

bool rampTrajectoryGroup::updateBestTrajectory(){
  const rampTrajectorySet::index<tag_cost>::type & cost_index = trajectoryContainer_.get<tag_cost>();
  
  if(!bestTrajectoryPtr){
    // In case that the pointer to the best path is NULL:
    bestTrajectoryPtr = *cost_index.begin();
    bestCost = bestTrajectoryPtr->cost;
    return true;  
  }
  // If it is evaluated already
  if (bestTrajectoryPtr == *cost_index.begin()){
    // If it is not changed
    return false;
  }else{
    // Yes, it is changed
    bestTrajectoryPtr = *cost_index.begin();
    bestCost = bestTrajectoryPtr->cost;
    return true;
  }
    
}

bool rampTrajectoryGroup::updateWorstTrajectory(){
  const rampTrajectorySet::index<tag_cost>::type & cost_index = trajectoryContainer_.get<tag_cost>();
  
  if(!worstTrajectoryPtr){
    // In case that the pointer to the best path is NULL:
    worstTrajectoryPtr = *cost_index.end();
    worstCost = worstTrajectoryPtr->cost;
    return true;  
  }
  // If it is evaluated already
  if (worstTrajectoryPtr == *cost_index.end()){
    // If it is not changed
    return false;
  }else{
    // Yes, it is changed
    worstTrajectoryPtr = *cost_index.end();
    worstCost = worstTrajectoryPtr->cost;
    return true;
  }
    
}

void rampTrajectoryGroup::updateTrajectoryContainer(
						    ){

  // Go through all the indicies
  rampTrajectorySet::index<tag_cost>::type & cost_index = trajectoryContainer_.get<tag_cost>();
  rampTrajectorySet::index<tag_id>::type & id_index = trajectoryContainer_.get<tag_id>();
  rampTrajectorySet::index<tag_feasibility>::type & feasibility_index = trajectoryContainer_.get<tag_feasibility>();
  for(rampTrajectorySet::iterator it_path = trajectoryContainer_.begin(); it_path != trajectoryContainer_.end();  std::advance(it_path, 1)){
      
    // (0) id
    rampTrajectorySet::index<tag_id>::type::iterator id_it = id_index.find((*it_path)->signature);
    id_index.modify(id_it, update_trajectory_id_());

    // (1) cost
    rampTrajectorySet::index_const_iterator<tag_cost>::type cost_it = trajectoryContainer_.project<tag_cost>(id_it ); 
    cost_index.modify(cost_it, update_trajectory_cost_());
      
    // (2) feasibility
    rampTrajectorySet::index_const_iterator<tag_feasibility>::type feasibility_it = trajectoryContainer_.project<tag_feasibility>(id_it);
    feasibility_index.modify(feasibility_it, update_trajectory_feasibility_());
  }
  // loop over the container
}
void rampTrajectoryGroup::updateOneTrajectory(const boost::shared_ptr<rampTrajectory> newptr 
					){
  // find the iterator to the path
  const rampTrajectorySet::iterator it_path = trajectoryContainer_.iterator_to(newptr);


  // (0) unique_hashed_id
  rampTrajectorySet::index<tag_id>::type & id_index = trajectoryContainer_.get<tag_id>();
  // use the old 'id' to find the path
  rampTrajectorySet::index<tag_id>::type::iterator id_it = id_index.find((*it_path)->signature);
  // update the id
  id_index.modify(id_it, update_trajectory_id_());

  
  // (1) non_unique cost
  rampTrajectorySet::index<tag_cost>::type & cost_index = trajectoryContainer_.get<tag_cost>();
  rampTrajectorySet::index_const_iterator<tag_cost>::type cost_it = trajectoryContainer_.project<tag_cost>(id_it  );
  cost_index.modify(cost_it, update_trajectory_cost_());
  
  // (2) keep the random_access: pass

  // (3) composite key will be updated automatically once the 'feasibility' and 'cost' indicies are changed. 
  
  // (4) feasibility
  rampTrajectorySet::index<tag_feasibility>::type & feasibility_index = trajectoryContainer_.get<tag_feasibility>();
  rampTrajectorySet::index_const_iterator<tag_feasibility>::type feasibility_it = trajectoryContainer_.project<tag_feasibility>(id_it);
  feasibility_index.modify(feasibility_it, update_trajectory_feasibility_());
  
}
