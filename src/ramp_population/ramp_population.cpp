/*
 * ramp_population.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_population/ramp_population.h"

bool rampPopulation::isUniqueTrajectory(const boost::shared_ptr<rampTrajectory> new_trajectory_ptr) const{
  
  // loop through all the groups
  for (
	   trajectoryGroupContrainer::iterator 
	 it = groupContainer_.begin() ; it != groupContainer_.end(); std::advance(it, 1) ){
	// If it is contained in any of the group, we return false
	if (!(*it)->isUniqueTrajectory(new_trajectory_ptr))
	  return false;
  }
  return true;
  // return the hashtable check result
  
}

bool rampPopulation::updateStartingState(
					 const robot_state::RobotStatePtr sample_state_ptr
					 ){

  // Check the consistency with the problem definition ? 
	
  
  // Go through the path in the array/container
  for (
	   trajectoryGroupContrainer::iterator 
	 it = groupContainer_.begin() ; it != groupContainer_.end(); std::advance(it, 1) ){
	(*it)->updateStartingState(sample_state_ptr);
  }

}


boost::shared_ptr<rampTrajectory> rampPopulation::calcBestTrajectory(){

  // 1. update the best feasible/infeasible trajectory in each subgroup
  // 2. update the subgroup best hash
  trajectoryGroupContrainer::index<tag_best>::type & best_index = groupContainer_.get<tag_best>();
  trajectoryGroupContrainer::index<tag_worst>::type & worst_index = groupContainer_.get<tag_worst>();


  for (
	   trajectoryGroupContrainer::iterator 
	 it_group = groupContainer_.begin() ; it_group != groupContainer_.end(); std::advance(it_group, 1) ){

	// best
	trajectoryGroupContrainer::index<tag_best>::type::iterator best_it = best_index.find((*it_group)->bestCost);
	best_index.modify(best_it, update_group_best_cost_());
	// worst
	trajectoryGroupContrainer::index_const_iterator<tag_worst>::type worst_it = groupContainer_.project<tag_worst>(best_it ); 
	worst_index.modify(worst_it, update_group_worst_cost_());

  }
  
  updateBestTrajectory();
  updateWorstTrajectory();
  
  return bestTrajectoryPtr;

}


bool rampPopulation::updateBestTrajectory(){
  // locate the trajectory group with the best cost 
  const trajectoryGroupContrainer::index<tag_best>::type & cost_index = groupContainer_.get<tag_best>();
  
  if(!bestTrajectoryPtr){
	// In case that the pointer to the best trajectory is NULL:
	bestTrajectoryPtr = (*cost_index.begin())->bestTrajectoryPtr;
	bestCost = bestTrajectoryPtr->cost;

	// bestCost = (*cost_index.begin())->cost;
	
	return true;  
  }
  // If it is evaluated already
  if (bestTrajectoryPtr == (*cost_index.begin())->bestTrajectoryPtr){
	// If it is not changed
	return false;
  }else{
	// Yes, it is changed
	bestTrajectoryPtr = (*cost_index.begin())->bestTrajectoryPtr;
	bestCost = bestTrajectoryPtr->cost;
	return true;
  }
	
}

bool rampPopulation::updateWorstTrajectory(){
  const trajectoryGroupContrainer::index<tag_worst>::type & cost_index =  groupContainer_.get<tag_worst>();
  
  if(!worstTrajectoryPtr){
	// In case that the pointer to the best trajectory is NULL:
	worstTrajectoryPtr = (*cost_index.end())->worstTrajectoryPtr;
	worstCost = worstTrajectoryPtr->cost;
	return true;  
  }
  // If it is evaluated already
  if (worstTrajectoryPtr == (*cost_index.end())->worstTrajectoryPtr){
	// If it is not changed
	return false;
  }else{
	// Yes, it is changed
	worstTrajectoryPtr = (*cost_index.end())->worstTrajectoryPtr;
	worstCost = worstTrajectoryPtr->cost;
	return true;
  }
	
}

int rampPopulation::calcTrajectorySubGroupNumber_(const rampTrajectory* newTrajectoryPtr) const {

  double trajectoryMeasure =  diversityMeasureCalculatorPtr_->trajectoryMeasureCalc(newTrajectoryPtr);

  //How to infer the group number based on the pathMeasure?
  
  int groupNumber(10);
  
  return groupNumber;
  
}

void rampPopulation::buildPopulation(boost::shared_ptr<rampSetting> &r_set_ptr)
{	
	r_set_ptr->setDefaultJointConfiguration();
	armCfg default_cfg;
	r_set_ptr->getCurrentJointConfiguration(default_cfg);
	/*
		construct trajectories and initialize their start and end
		configs to default configurations
	*/
	for (int i=0; i<num_of_trajs; ++i)
	{
		rampTrajectoryPtr tmp_traj = boost::make_shared<rampTrajectory>(r_set_ptr);
		tmp_traj->setTrajectory(default_cfg, default_cfg, i);
		trajectories.push_back(tmp_traj);
	}
}

void rampPopulation::initializePopulation(boost::shared_ptr<rampOperators> &r_oprt_ptr)
{
	/*
		Step 1
		for each trajectory in population
		insert 10 knot cfgs
	*/
	for (int i=0; i<num_of_trajs; ++i)
	{
		r_oprt_ptr->registerTrajectory(trajectories.at(i));
		for(int j=0; j<5; ++j) 
		{
			r_oprt_ptr->applyInsert();
		}
		r_oprt_ptr->dropTrajectory();
	}

	/*
		Step 2
		compute velocities and time stamps for each trajectory
	*/
	int nDofs = trajectories.front()->getNumOfDofs();
	std::vector<double> start_end_vel(nDofs, 0.0);
	for (int i=0; i<num_of_trajs; ++i)
	{
		trajectories.at(i)->generateTrajectoryFromPathAnalytically(start_end_vel, start_end_vel);
	}
}

void rampPopulation::evaluateTrajectoriesInPopulation()
{
	for (int i=0; i<num_of_trajs; ++i)
	{
		trajectories.at(i)->collisionChecking();
		trajectories.at(i)->evaluateTrajectory();
	}
}

void rampPopulation::printPopulationInfo()
{
	for (int i=0; i<num_of_trajs; ++i)
	{
		trajectories.at(i)->convertToTrajectoryMsgs();
		trajectories.at(i)->printTrajectoryInfo();
	}
}




