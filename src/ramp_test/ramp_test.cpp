/*
 * ramp_test.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

 #include "ramp_test/ramp_test.h"

rampTest::rampTest()
{

}

void rampTest::setRampSettingPtr(boost::shared_ptr<rampSetting> &r_set_p)
{
	ramp_setting_ptr = r_set_p;
}

void rampTest::setRampSamplingPtr(boost::shared_ptr<rampSampling> &r_samp_p)
{
	ramp_sampling_ptr = r_samp_p;
}

void rampTest::setRampTrajectoryPtr(boost::shared_ptr<rampTrajectory> &r_traj_p)
{
	ramp_traj_ptr = r_traj_p;
}

void rampTest::setRampOperatorsPtr(boost::shared_ptr<rampOperators> &r_oprt_p)
{
	ramp_oprt_ptr = r_oprt_p;
}

void rampTest::setRampRenderingPtr(boost::shared_ptr<rampRendering> &r_rdr_p)
{
	ramp_rdr_ptr = r_rdr_p;
}

void rampTest::testRampSetting()
{
	// set robot state for internal representation kinematic_state
	ramp_setting_ptr->setDefaultJointConfiguration();
	// ramp_setting->setRandomJointConfiguration();

	// get current state of internal representation kinematic_state
	armCfg jnt_vals;
	ramp_setting_ptr->getCurrentJointConfiguration(jnt_vals);
	int nDofs = 0;
	ramp_setting_ptr->getNumberOfDofs(nDofs);

	// get joint bounds for the current kinematic chain
	ramp_setting_ptr->printJointBoundsInfo();

	// get eef pose of internal representation kinematic_state
	geometry_msgs::Pose eef_pose;
	ramp_setting_ptr->getEefPose(eef_pose);
	ROS_INFO("Current EEF pose:");
	rampUtil::printGeoMsgPose(eef_pose);
}

void rampTest::testRampSampling()
{
	geometry_msgs::Pose eef_pose;
	ramp_setting_ptr->getEefPose(eef_pose);

	armCfg ik_jv;
	ramp_sampling_ptr->sampleJntCfgGivenEefPose(ik_jv, eef_pose);
	rampUtil::printArmCfg(ik_jv);

	std::vector<armCfg> ik_jvs;
	ramp_setting_ptr->getAllIKJointConfigurations(ik_jvs, eef_pose);
}

void rampTest::testRampTrajectory()
{
	ramp_setting_ptr->setDefaultJointConfiguration();
	armCfg jnt_vals;
	ramp_setting_ptr->getCurrentJointConfiguration(jnt_vals);
	ramp_traj_ptr->setTrajectory(jnt_vals,jnt_vals, 1);
	
	// append two knot cfgs for testing purposes
	int nDofs = 0;
	ramp_setting_ptr->getNumberOfDofs(nDofs);
	jnt_vals = armCfg(nDofs,0.5);
	ramp_traj_ptr->knot_cfgs.push_back(jnt_vals);
	jnt_vals = armCfg(nDofs,1.0);
	ramp_traj_ptr->knot_cfgs.push_back(jnt_vals);
	jnt_vals = armCfg(nDofs,1.5);
	ramp_traj_ptr->knot_cfgs.push_back(jnt_vals);

	/* Instantiate rampOperators*/
	rampOperators ramp_oprt(ramp_sampling_ptr);
	ramp_oprt.registerTrajectory(ramp_traj_ptr);
	// for (int i=0; i<20; ++i)
	// {
	// 	ramp_oprt.applyInsert();
	// }
	// ramp_oprt.applyInsert();
	// ramp_oprt.applyDelete();
	// ramp_oprt.applyChange();
	// ramp_oprt.applyInsert();
	// ramp_oprt.applySwap();

	// duplicate trajectory to test cross over operator
	// rampTrajectory ramp_traj_2 = *ramp_traj_ptr;
	// ramp_oprt.applyCrossOver(ramp_traj_2);

	// test trajectory generation with moveit trajectory_processing class
	std::vector<double> cfg_v(nDofs, 0.0);
	using namespace std::chrono;
	high_resolution_clock::time_point timer1 = high_resolution_clock::now();
	// ramp_traj_ptr->generateTrajectoryFromPathNumerically(cfg_v);
	high_resolution_clock::time_point timer2 = high_resolution_clock::now();
	ramp_traj_ptr->generateTrajectoryFromPathAnalytically(cfg_v, cfg_v);
	high_resolution_clock::time_point timer3 = high_resolution_clock::now();

  	duration<double> time_span1 = duration_cast<duration<double>>(timer2 - timer1);
  	duration<double> time_span2 = duration_cast<duration<double>>(timer3 - timer2);
	ROS_INFO("Time parameterization took %f secs (numerically) and %f secs (analytically)", time_span1.count(), time_span2.count());

	// ramp_oprt.applyStop(); // should only be called after generateTrajectoryFromPath()
	ramp_oprt.dropTrajectory();

	// armCfg test;
	// ramp_traj_ptr->getJntCfgAtTimeT(0.01,test,"analytically");
	// rampUtil::printArmCfg(test);
	// ramp_traj_ptr->getJntCfgAtTimeT(0.01,test,"numerically");
	// rampUtil::printArmCfg(test);
	// ramp_traj_ptr->getJntCfgAtTimeT(0.915,test,"analytically");
	// rampUtil::printArmCfg(test);
	// ramp_traj_ptr->getJntCfgAtTimeT(0.915,test,"numerically");
	// rampUtil::printArmCfg(test);
	// ramp_traj_ptr->getJntCfgAtTimeT(0.917,test,"analytically");
	// rampUtil::printArmCfg(test);
	// ramp_traj_ptr->getJntCfgAtTimeT(1.375,test,"analytically");
	// rampUtil::printArmCfg(test);
	// ramp_traj_ptr->getJntCfgAtTimeT(1.375,test,"numerically");
	// rampUtil::printArmCfg(test);
	// ramp_traj_ptr->getJntCfgAtTimeT(2.73,test,"analytically");
	// rampUtil::printArmCfg(test);
	// ramp_traj_ptr->getJntCfgAtTimeT(2.73,test,"numerically");
	// rampUtil::printArmCfg(test);

	// testRampRendering();
}

void rampTest::testRampEvaluationFunction()
{
	trajectory_msgs::JointTrajectory traj_msg;
	ramp_traj_ptr->convertToTrajectoryMsgs(traj_msg);
	ramp_traj_ptr->printTrajectoryInfo();
	
	ros::Time t1 = ros::Time::now();
	ramp_traj_ptr->evaluateTrajectory();
	ros::Time t2 = ros::Time::now();
	ros::Duration dur1 = t2-t1;
	ROS_INFO("trajectory evaluation took %f secs", dur1.toSec());
}

void rampTest::testRampRendering()
{
	// ramp_rdr_ptr->renderJointConfiguration(jnt_vals, ramp_setting->getJointNames());
	// ramp_rdr_ptr->renderJointTrajectory(*ramp_traj, ramp_setting->getJointNames());
	// ramp_rdr_ptr->renderJointTrajectoryWithInterpolation(*ramp_traj, ramp_setting->getJointNames());
	// ramp_rdr_ptr->renderJointTrajectoryMsg(*ramp_traj, ramp_setting->getJointNames());
	ramp_rdr_ptr->renderJointTrajectoryGazebo(*ramp_traj_ptr);
}

void rampTest::testConstrainedPose()
{
	geometry_msgs::Pose eef_pose;
	ramp_setting_ptr->getEefPose(eef_pose);

	std::vector<double> base_pose_vec(6,0.0);
	base_pose_vec[0] = eef_pose.position.x-0.1;
	base_pose_vec[1] = eef_pose.position.y;
	base_pose_vec[2] = eef_pose.position.z;
	base_pose_vec[4] = 0.5*M_PI;

	// set eef to this base pose
	ROS_INFO("set eef to base pose");
	geometry_msgs::Pose tgt_pose;
	rampUtil::convertXYZRPYtoQuaternion(base_pose_vec,tgt_pose);
	armCfg ik_jv;
	ramp_sampling_ptr->sampleJntCfgGivenEefPose(ik_jv, tgt_pose);
	ramp_setting_ptr->setJointConfiguration(ik_jv);
	ramp_rdr_ptr->renderJointConfiguration(ik_jv, ramp_setting_ptr->getJointNames());

	ROS_INFO("sleeping for 15 secs");
	sleep(15.0);

	// get a constrained pose satisfying soft constraints
	ROS_INFO("set eef to a pose under soft constraints");
	std::string axis("X");
	constrainedPose cp(base_pose_vec, axis, 0.5*M_PI);
	geometry_msgs::Pose cstr_pose;
	cp.getOneConstrainedPose(cstr_pose);
	rampUtil::printGeoMsgPose(cstr_pose);
	ramp_sampling_ptr->sampleJntCfgGivenEefPose(ik_jv, cstr_pose);
	rampUtil::printArmCfg(ik_jv);
	ramp_rdr_ptr->renderJointConfiguration(ik_jv, ramp_setting_ptr->getJointNames());
}

void rampTest::testCollisionChecking(ros::NodeHandle n)
{
	ros::Publisher planning_scene_diff_publisher = n.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
	ros::ServiceClient planning_scene_diff_client = n.serviceClient<moveit_msgs::ApplyPlanningScene>("apply_planning_scene");
	sleep(2.0); // has to sleep for a few seconds to set up the publisher connection

	// add a collision object
	moveit_msgs::AttachedCollisionObject attached_object;
	attached_object.link_name = "base_link";
	attached_object.object.header.frame_id = "base_link";
	attached_object.object.id = "box";
	geometry_msgs::Pose pose;
	pose.position.x = 0.0;
	pose.position.y = 0.3;
	pose.position.z = 0.3;
	pose.orientation.w = 1.0;
	shape_msgs::SolidPrimitive primitive;
	primitive.type = primitive.BOX;  
	primitive.dimensions.resize(3);
	primitive.dimensions[0] = 0.1;
	primitive.dimensions[1] = 0.1;
	primitive.dimensions[2] = 0.1;  
	attached_object.object.primitives.push_back(primitive);
	attached_object.object.primitive_poses.push_back(pose);
	attached_object.object.operation = attached_object.object.ADD;

	moveit_msgs::PlanningScene planning_scene;
	planning_scene.world.collision_objects.push_back(attached_object.object);
	planning_scene.is_diff = true;

	// method 1 - asynchrounous
	// planning_scene_diff_publisher.publish(planning_scene); 

	// method 2 - synchronous
	planning_scene_diff_client.waitForExistence();
	moveit_msgs::ApplyPlanningScene srv;
	srv.request.scene = planning_scene;
	planning_scene_diff_client.call(srv);

	using namespace std::chrono;
	high_resolution_clock::time_point timer1 = high_resolution_clock::now();
	double col_time = 0.0;
	std::string col_obj_id; 
	armCfg col_cfg;
	ramp_traj_ptr->collisionChecking(col_time, col_obj_id, col_cfg);
	high_resolution_clock::time_point timer2 = high_resolution_clock::now();

	ramp_rdr_ptr->renderJointConfiguration(col_cfg, ramp_setting_ptr->getJointNames());

	duration<double> time_span1 = duration_cast<duration<double>>(timer2 - timer1);
	ROS_INFO("Trajectory collison checking took %f secs", time_span1.count());

	// if (ramp_setting_ptr->isRobotColliding())
	// 	ROS_INFO("The robot is colliding.");
	// else
	// 	ROS_INFO("The robot is not colliding");

	// remove box from planning scene
	// moveit_msgs::CollisionObject remove_object;
	// remove_object.id = "box";
	// remove_object.header.frame_id = "odom_combined";
	// remove_object.operation = remove_object.REMOVE;
	// ROS_INFO("Removing box from the world.");
	// planning_scene.world.collision_objects.clear();
	// planning_scene.world.collision_objects.push_back(remove_object);
	// planning_scene_diff_publisher.publish(planning_scene);
	// if (ramp_setting_ptr->isRobotColliding())
	// 	ROS_INFO("The robot is colliding.");
	// else
	// 	ROS_INFO("The robot is not colliding");
}

void rampTest::testRampObstacle(ros::NodeHandle n)
{
	boost::shared_ptr<ros::ServiceClient> psdc_ptr;
	psdc_ptr = boost::make_shared<ros::ServiceClient>(n.serviceClient<moveit_msgs::ApplyPlanningScene>("apply_planning_scene"));
	geometry_msgs::Pose pose;
	pose.position.x = 0.3;
	pose.position.y = 0.2;
	pose.position.z = 0.5;
	pose.orientation.w = 1.0;
	Eigen::Vector3f dim;
	dim << 0.1, 0.1, 0.1; 
	std::string type("box");
	std::string id("box");
	rampObstacle rob(type, id, pose, dim, psdc_ptr);

	using namespace std::chrono;
	high_resolution_clock::time_point timer1 = high_resolution_clock::now();
	double col_time = 0.0;
	std::string col_obj_id; 
	armCfg col_cfg;
	ramp_traj_ptr->collisionChecking(col_time, col_obj_id, col_cfg);
	high_resolution_clock::time_point timer2 = high_resolution_clock::now();

	ramp_rdr_ptr->renderJointConfiguration(col_cfg, ramp_setting_ptr->getJointNames());

	duration<double> time_span1 = duration_cast<duration<double>>(timer2 - timer1);
	ROS_INFO("Trajectory collison checking took %f secs", time_span1.count());

	pose.position.x = 0.2;	
	pose.position.y = 0.3;	
	pose.position.z = 0.3;	
	// rob.setNewPose(pose);
	// sleep(2.0);
	// ramp_traj_ptr->collisionChecking(col_time, col_obj_id, col_cfg);
	// ramp_rdr_ptr->renderJointConfiguration(col_cfg, ramp_setting_ptr->getJointNames());

	sleep(10.0);
	rob.clear();

}


