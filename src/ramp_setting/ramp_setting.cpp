/*
 * ramp_setting.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_setting/ramp_setting.h"

rampSetting::rampSetting()
:robot_model_loader("robot_description")
{
	// specify joint names (getter function from joint 
	// model group may include unecessary joint names)

	/* Barrett WAM */
	// joint_names.push_back("wam/base_yaw_joint");
	// joint_names.push_back("wam/shoulder_pitch_joint");
	// joint_names.push_back("wam/shoulder_yaw_joint");
	// joint_names.push_back("wam/elbow_pitch_joint");
	// joint_names.push_back("wam/wrist_yaw_joint");
	// joint_names.push_back("wam/wrist_pitch_joint");
	// joint_names.push_back("wam/palm_yaw_joint");

	// joint_group_name = std::string("arm");

	/* ABB IRB 120*/
	joint_names.push_back("joint_1");
	joint_names.push_back("joint_2");
	joint_names.push_back("joint_3");
	joint_names.push_back("joint_4");
	joint_names.push_back("joint_5");
	joint_names.push_back("joint_6");

	joint_group_name = std::string("manipulator");
	eef_link_name = std::string("link_6");

	loadRobotModel();
	setJointBoundsMap();
}

void rampSetting::loadRobotModel()
{
	kinematic_model = robot_model_loader.getModel();
	kinematic_state = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model));
	joint_model_group = kinematic_model->getJointModelGroup(joint_group_name);

	planning_scene_monitor = planning_scene_monitor::PlanningSceneMonitorPtr(new planning_scene_monitor::PlanningSceneMonitor("robot_description"));
	kine_metrics_ptr = boost::make_shared<kinematics_metrics::KinematicsMetrics>(kinematic_model);
}

void rampSetting::setDefaultJointConfiguration()
{
	kinematic_state->setToDefaultValues();
}

void rampSetting::setRandomJointConfiguration()
{
	kinematic_state->setToRandomPositions();
}

void rampSetting::setJointConfiguration(const armCfg &target_cfg)
{
	kinematic_state->setJointGroupPositions(joint_model_group, target_cfg);
}

void rampSetting::getCurrentJointConfiguration(armCfg &joint_values)
{
	kinematic_state->copyJointGroupPositions(joint_model_group, joint_values);
}

void rampSetting::getEefPose(geometry_msgs::Pose &eef_pose)
{
	const Eigen::Affine3d &eef_tf = kinematic_state->getGlobalLinkTransform(eef_link_name);

	// convert eigen::affine3d to geometry_msgs::Pose
	rampUtil::convertAffine3dtoQuaternion(eef_tf, eef_pose);
}

void rampSetting::getEefPose(Eigen::Affine3d &eef_pose)
{
	eef_pose = kinematic_state->getGlobalLinkTransform(eef_link_name);
}

bool rampSetting::getIKJointConfiguration(armCfg &joint_values, const geometry_msgs::Pose &eef_pose)
{
	const kinematics::KinematicsBaseConstPtr& solver = joint_model_group->getSolverInstance();
	armCfg cur_cfg;
	getCurrentJointConfiguration(cur_cfg);
	moveit_msgs::MoveItErrorCodes error;
	if ( solver->getPositionIK(eef_pose, cur_cfg, joint_values, error) )
		return true;
	else 
		return false;
}

bool rampSetting::getAllIKJointConfigurations(std::vector<armCfg> &vec_joint_values, const geometry_msgs::Pose &eef_pose)
{
	const kinematics::KinematicsBaseConstPtr& solver = joint_model_group->getSolverInstance();
	int nd = 0;
	getNumberOfDofs(nd);
	armCfg dummy_seed_cfg(nd, 0.0);
	std::vector<geometry_msgs::Pose> ik_poses;
	ik_poses.push_back(eef_pose);
	kinematics::KinematicsResult result;
	vec_joint_values.clear();
	if ( solver->getPositionIK(ik_poses, dummy_seed_cfg, vec_joint_values, result,kinematics::KinematicsQueryOptions()) )
	{
		for (int i=0; i<vec_joint_values.size(); ++i)
		{
			#ifdef DEBUG
			std::cout << "IK solutions are " << i << std::endl;
			std::cout << "[ ";
			for (int j=0; j<vec_joint_values.at(i).size(); ++j)
			{
				std::cout << vec_joint_values.at(i).at(j) << " ";
			}
			std::cout << "]" << std::endl;
			#endif
		}
		return true;
	}
	else 
		return false;
}

void rampSetting::getNumberOfDofs(int &numOfDofs)
{
	armCfg jv;
	kinematic_state->copyJointGroupPositions(joint_model_group, jv);
	numOfDofs = jv.size();
	#ifdef DEBUG
	ROS_INFO("This kinematic chain has %d DOFs", numOfDofs);
	#endif
}

robot_model::RobotModelPtr rampSetting::getRobotModelPtr()
{
	return kinematic_model;
}

void rampSetting::setJointNames(std::vector<std::string> jnt_names)
{
	joint_names.clear();
	joint_names = jnt_names;
}

std::vector<std::string> rampSetting::getJointNames()
{
	return joint_names;
}

std::string rampSetting::getJointModelGroupName()
{
	return joint_group_name;
}

void rampSetting::setJointBoundsMap()
{
	for (int i=0; i<joint_names.size(); ++i)
	{
		joint_bounds_maps[joint_names.at(i)] = kinematic_model->getVariableBounds(joint_names.at(i));
	}
}

const moveit::core::VariableBounds rampSetting::getJointBounds(const std::string joint_name)
{
	return joint_bounds_maps.find(joint_name)->second;
}

void rampSetting::printJointBoundsInfo()
{
	for (int i=0; i<joint_names.size(); ++i)
	{
		ROS_INFO("Joint %s bounds are:", joint_names.at(i).c_str());
		const moveit::core::VariableBounds& tmp = getJointBounds(joint_names.at(i));
		if (tmp.position_bounded_)
			ROS_INFO("Position [%f, %f]", tmp.min_position_, tmp.max_position_);
		else
			ROS_WARN("Position is not bounded.");

		if (tmp.velocity_bounded_)
			ROS_INFO("Velocity [%f, %f]", tmp.min_velocity_, tmp.max_velocity_);
		else
			ROS_WARN("Velocity is not bounded.");

		if (tmp.acceleration_bounded_)
			ROS_INFO("Acceleration [%f, %f]", tmp.min_acceleration_, tmp.max_acceleration_);
		else
			ROS_WARN("Acceleration is not bounded.");
	}
}


bool rampSetting::isRobotColliding(const armCfg &cfg)
{
	setJointConfiguration(cfg);
	/* most time-consuming part */
	// seems that this is still necessary
	planning_scene_monitor->requestPlanningSceneState(); 

	planning_scene_monitor::LockedPlanningSceneRO ps(planning_scene_monitor);
	int method = 1;
	bool isColliding = false;
	switch (method)
	{
		case 1:
		{
			if (ps->isStateColliding(*kinematic_state))
				isColliding = true;
			break;
		}
		case 2:
		{
			collision_detection::CollisionResult::ContactMap contacts;
			ps->getCollidingPairs(contacts, *kinematic_state);
			if (contacts.size()!=0)
				isColliding = true;
			break;
		}
	}
	return isColliding;
}

double rampSetting::getManipulabilityIndex(const armCfg &cfg)
{
	setJointConfiguration(cfg);
	double manipulability_index = 0.0;
	bool status = false;

	int method = 2;
	// seems that method 1 and 2 have similar run time speeds
	switch (method)
	{
		case 1:
		{
			Eigen::MatrixXd jacobian = kinematic_state->getJacobian(joint_model_group);
			Eigen::MatrixXd matrix = jacobian*jacobian.transpose();
			manipulability_index = sqrt(matrix.determinant());
			status = true;
			break;
		}
		case 2:
		{
			// this is an expensive operation...
			kinematic_state->update();
			status = kine_metrics_ptr->getManipulabilityIndex(*kinematic_state,
												  		 	  joint_model_group,
													 		  manipulability_index);
			break;
		}
	}
	
	if (status)
		return manipulability_index;
	else
	{
		ROS_ERROR("Failed to compute manipulability index");
		return -1.0;
	}
}

rampSetting::~rampSetting()
{

}

