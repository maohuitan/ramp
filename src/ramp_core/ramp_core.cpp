/*
 * ramp_core.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_core/ramp_core.h"

rampCore::rampCore()
{
	srand(time(NULL));
	stats.num_of_ctrl_cyc = 0;
	stats.num_of_plan_cyc = 0;

	robot_state_topic = "/joint_states";
	robot_state_sub = nh.subscribe(robot_state_topic, 1, &rampCore::robotStateCallback, this);
}

void rampCore::robotStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
	robot_cfg = msg->position;
	rampUtil::printArmCfg(robot_cfg);
}

void rampCore::initialize()
{
	ROS_INFO("Initializing RAMP...");
	/*
		Step 1
		Initialize kinematic setting and sampler
		rampSetting - load in robot kinematics 
		rampSampling - sampler of random/eef-constrained joint configurations
	*/
	ROS_INFO("Loading robot kinematics and robot state sampler...");
	ramp_setting_ptr = boost::make_shared<rampSetting>();
	ramp_sampling_ptr = boost::make_shared<rampSampling>(ramp_setting_ptr);

	/*
		Step 2
		Initialize operators
	*/
	ROS_INFO("Initializing RAMP genetic operators...");
	ramp_oprt_ptr = boost::make_shared<rampOperators>(ramp_sampling_ptr);

	/*
		Step 3
		Initialize population
	*/
	ROS_INFO("Initializing RAMP population...");
	ramp_population_ptr = boost::make_shared<rampPopulation>();
	ramp_population_ptr->buildPopulation(ramp_setting_ptr);
	ramp_population_ptr->initializePopulation(ramp_oprt_ptr);

	/*
		Step 3
		Set the goal of this planning
	*/
	ROS_INFO("Set up RAMP goal...");
	std::vector<double> pose_vec(6,0.0);
	pose_vec[0] = 0.3;
	pose_vec[1] = 0.3;
	pose_vec[2] = 0.3;
	pose_vec[3] = 0.;
	pose_vec[4] = 0.;
	pose_vec[5] = 0.;

	geometry_msgs::Pose goal;
	rampUtil::convertXYZRPYtoQuaternion(pose_vec, goal);
	ramp_goal.setGoalEefPose(goal);

	/*
		Step 4
		Add obstacles
	*/
	ROS_INFO("Setting up RAMP obstacles...");
	boost::shared_ptr<ros::ServiceClient> psdc_ptr;
	psdc_ptr = boost::make_shared<ros::ServiceClient>(nh.serviceClient<moveit_msgs::ApplyPlanningScene>("apply_planning_scene"));
	geometry_msgs::Pose obs_pose;
	obs_pose.position.x = 0.3;
	obs_pose.position.y = 0.2;
	obs_pose.position.z = 0.5;
	obs_pose.orientation.w = 1.0;
	Eigen::Vector3f dim;
	dim << 0.1, 0.1, 0.1; 
	std::string type("box");
	std::string id("box");
	rampObstacle ob1(type, id, obs_pose, dim, psdc_ptr);

	/*
		Step 5
		Initialize sensing and add obstacles
	*/
	ROS_INFO("Initializing RAMP sensing...");
	ramp_sensing_ptr = boost::make_shared<rampSensing>(nh);
	ramp_sensing_ptr->addObstacle(ob1);

	/*
		Step 6
		Initialize control/rendering
	*/
	ROS_INFO("Set up RAMP control...");
	ramp_rendering_ptr = boost::make_shared<rampRendering>(nh);
	ROS_INFO("RAMP Initialization Done.");

	/*
		Step 7
		Initial evaluation of trajectories in population
	*/
	ramp_population_ptr->evaluateTrajectoriesInPopulation();
}

void rampCore::showPopulation()
{
	ramp_population_ptr->printPopulationInfo();
}

void rampCore::do_control()
{
	// 1-update start config and velocity for each trajectory 
	// 2-re-generate trajectory
	// 3-re-evaluate trajectory

	boost::shared_ptr<rampTrajectory> exe_traj = ramp_population_ptr->calcBestTrajectory();
	ramp_rendering_ptr->renderJointTrajectoryGazebo(*exe_traj);
}

void rampCore::do_planning()
{	
	// should let ramp_population_ptr return a traj to work on 
	boost::shared_ptr<rampTrajectory> traj = ramp_population_ptr->calcBestTrajectory();
	ramp_oprt_ptr->registerTrajectory(traj);
	// pick an operator 
	int operator_ind = rand() % 5 + 1; // consider 5 operators for now

	switch (operator_ind)
	{
		case 1:
		{
			ramp_oprt_ptr->applyInsert();
			break;
		}
		case 2:
		{
			ramp_oprt_ptr->applyDelete();
			break;
		}
		case 3:
		{
			ramp_oprt_ptr->applyChange();
			break;
		}
		case 4:
		{
			ramp_oprt_ptr->applySwap();
			break;
		}
		case 5:
		{
			// note this should randomly return a trajectory (not the best trajectory)
			boost::shared_ptr<rampTrajectory> traj2 = ramp_population_ptr->calcBestTrajectory();
			ramp_oprt_ptr->applyCrossOver(*traj2);
			break;
		}
	}
	ramp_oprt_ptr->dropTrajectory();
	traj->evaluateTrajectory();
	if (traj->cost < ramp_population_ptr->worstCost)
	{
		traj->collisionChecking();
		if (traj->feasibility)
		{
			// randomly replace a feasible trajectory(not best) in population
		}
		else
		{
			// randomly replace an infeasible trajectory(not best) in population
		}
	}
}

void rampCore::run()
{
	double control_freq = 20.0; // 20Hz
	ros::Time cur_time = ros::Time::now();
	ros::Time last_control_time = ros::Time::now();
	int count = 0;
	geometry_msgs::Pose eef_pose;
	while(ros::ok() && !ramp_goal.isGoalReached())
	{
		/*
			step 1
			do planning cycle once
		*/
		// do_planning();
		// ++stats.num_of_plan_cyc;

		/*
			step 2
			do control cycle once
		*/
		// cur_time = ros::Time::now();
		// if ( (cur_time-last_control_time).toSec() > (1.0/control_freq) || count==0)
		// {
		// 	last_control_time = ros::Time::now();
		// 	do_control();
		// 	++stats.num_of_ctrl_cyc;
		// }
		// ++count;

		/*
			step 3
			process latest sensing information if it arrives from sensing topics
		*/
		ros::spinOnce(); // process callbacks for sensing cycle 1) update obstacles info 2) update robot state

		/*
			step 4
			update robot current state 
		*/
		// ramp_setting_ptr->setJointConfiguration(robot_cfg);
		// ramp_setting_ptr->getEefPose(eef_pose);
		// ramp_goal.setCurrentEefPose(eef_pose);
	}

	if (ramp_goal.isGoalReached())
		ROS_INFO("Goal reached!");
	else
		ROS_ERROR("RAMP main loop stopped and failed to reach the goal.");
}

void test(ros::NodeHandle n)
{
	// instantitate objects
	boost::shared_ptr<rampSetting> ramp_setting;
	ramp_setting = boost::make_shared<rampSetting>();

	boost::shared_ptr<rampSampling> ramp_sampling;
	ramp_sampling = boost::make_shared<rampSampling>(ramp_setting);

	boost::shared_ptr<rampTrajectory> ramp_traj;
	int nDofs = 0;
	ramp_setting->getNumberOfDofs(nDofs);
	ramp_traj = boost::make_shared<rampTrajectory>(ramp_setting);

	boost::shared_ptr<rampOperators> ramp_operators;
	ramp_operators = boost::make_shared<rampOperators>(ramp_sampling);

	boost::shared_ptr<rampRendering> ramp_rdr;
	ramp_rdr = boost::make_shared<rampRendering>(n);

	// instantiate rampTest
	rampTest rt;
	rt.setRampSettingPtr(ramp_setting);
	rt.setRampSamplingPtr(ramp_sampling);
	rt.setRampTrajectoryPtr(ramp_traj);
	rt.setRampOperatorsPtr(ramp_operators);
	rt.setRampRenderingPtr(ramp_rdr);

	// test
	// rt.testRampSetting();
	// rt.testRampSampling();
	rt.testRampTrajectory();
	// rt.testConstrainedPose();
	// rt.testCollisionChecking(n);
	rt.testRampObstacle(n);
	rt.testRampEvaluationFunction();
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ramp_node");
	ros::NodeHandle n;
	// ros::AsyncSpinner spinner(4);
	// spinner.start();

	ROS_INFO("Sleeping to wait for MoveIt and Rviz setup...");
	sleep(15.0);
	ROS_INFO("***************************************************");
	ROS_INFO("Real-time Adaptive Motion Planning for Manipulators");
	ROS_INFO("***************************************************");

	rampCore ramp_core;
	ramp_core.initialize();
	ramp_core.showPopulation();
	ramp_core.run();

	// ros::waitForShutdown();
	return 0;
}

