/*
 * ramp_trajectory.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

 #include "ramp_trajectory/ramp_trajectory.h"

rampTrajectory::rampTrajectory(boost::shared_ptr<rampSetting> &r_set_p)
{
	trajID = -1;
	n_config = 0;
	ramp_setting_ptr = r_set_p;
	nDofs = 0;
	ramp_setting_ptr->getNumberOfDofs(nDofs);
	jnt_names = ramp_setting_ptr->getJointNames();

	ROS_INFO("Trajectory data structure constructed with %d DOFs", nDofs);
	start_config = armCfg(nDofs,0.0);
	end_config = armCfg(nDofs,0.0);

	// read in joint velocity max
	double tmp_vmax = 0.;
	for (int i=0; i<jnt_names.size(); ++i)
	{
		tmp_vmax = ramp_setting_ptr->getRobotModelPtr()->getVariableBounds(jnt_names.at(i)).max_velocity_;
		jnt_vel_max.push_back(tmp_vmax);
	}

	// set cost function weights
	cost_weights.c1 = 0.33;
	cost_weights.c2 = 0.33;
	cost_weights.c3 = 0.33;

	// set cost function normalization factors
	cost_nrm_factors.a1 = 100.0;
	cost_nrm_factors.a2 = 100.0;
	cost_nrm_factors.a3 = 100.0;

	// set moment of inertia for each joint
	// use 1.0 for now as absolute values are not important
	joints_moment_of_inertia = std::vector<double>(nDofs, 1.0);

	// trajectory cost
	cost = 0.0; 
	feasibility = false; // false by default. will be set in collisionChecking()
	colliding_time_from_beginning = 0.0;
}

void rampTrajectory::setTrajectory(armCfg &start_cfg, armCfg &end_cfg, int trajectoryID)
{
	if (start_cfg.size()==nDofs && end_cfg.size()==nDofs)
	{
		start_config = start_cfg;
		end_config = end_cfg;
		trajID = trajectoryID;
	}
	else
		ROS_ERROR("Joint Configuration Dimension Mismatch in Setting Up Trajectory.");
}

void rampTrajectory::getJntNames(std::vector<std::string> &jnt_ns)
{
	jnt_ns = jnt_names;
}

int rampTrajectory::getNumOfDofs() const
{
	return nDofs;
}

int rampTrajectory::getTrajectoryID() const
{
	return trajID;
}

// time parametrization of a joint path
void rampTrajectory::generateTrajectoryFromPathNumerically(const std::vector<double> &start_cfg_vel)
{
	ROS_INFO("Generating trajectory %d with %zd knot cfgs from path numerically...", trajID, knot_cfgs.size());
	/********************************************* 
	  step 1
	  construct moveit_msgs::robotTrajectory
	  from start_config, knot_cfgs, end_config 
	*********************************************/
	trajectory_msgs::JointTrajectoryPoint jnt_pnt;
	moveit_msgs::RobotTrajectory traj_msg;

	traj_msg.joint_trajectory.joint_names = jnt_names;

	// start config
	jnt_pnt.positions = start_config;
	// start config velocity
	jnt_pnt.velocities = start_cfg_vel;
	traj_msg.joint_trajectory.points.push_back(jnt_pnt);

	// place holder to be filled in after time parameterization
	jnt_pnt.velocities = std::vector<double>(start_config.size(), 0.0);
	// knot configs
	for (std::size_t i=0; i<knot_cfgs.size(); ++i)
	{
		jnt_pnt.positions = knot_cfgs.at(i);
		traj_msg.joint_trajectory.points.push_back(jnt_pnt);
	}

	// end config
	jnt_pnt.positions = end_config;
	traj_msg.joint_trajectory.points.push_back(jnt_pnt);

	/*******************************************
	  step 2
	  Construct robot_trajector::robotTrajectory
	  from moveit_msgs::robotTrajectory
	********************************************/
	// make reference robot state
	robot_state::RobotStatePtr kine_state = robot_state::RobotStatePtr(new robot_state::RobotState(ramp_setting_ptr->getRobotModelPtr()));
	kine_state->setToRandomPositions();

	robot_trajectory::RobotTrajectory rob_traj(ramp_setting_ptr->getRobotModelPtr(), ramp_setting_ptr->getJointModelGroupName());
	rob_traj.setRobotTrajectoryMsg(*kine_state, traj_msg);

	#ifdef DEBUG
	// print way point in rob_traj
	std::vector<double> joint_values(nDofs, 0.0);
	for (std::size_t i=0; i<rob_traj.getWayPointCount(); ++i)
	{
		robot_state::RobotStatePtr tmp_state = rob_traj.getWayPointPtr(i);
		tmp_state->copyJointGroupPositions(joint_group_name, joint_values);
		for(int j=0; j<joint_values.size(); ++j)
		{
			std::cout << joint_values[j] << std::endl;
		}
		std::cout << std::endl;
	}
	#endif
	
	/******************************************************************************
	  step 3
	  Time-parameterize the path and get way 
	  point duration (time from previous state
	  to current state)
	  Method 1 - Iterative parabolic time parameterization
	  Method 2 - Spline parameterization (not too much difference in running time compared to Method 1)
	  It seems that method 2 has bugs and it is not maintained
	  after ROS Indigo anymore, so we should use Method 1 for now.
	  Method 1 is also the default method in MoveIt but it may 
	  generate non-smooth accelerations. See 
	  http://docs.ros.org/kinetic/api/moveit_tutorials/html/doc/time_parameterization_tutorial.html
	  Method 1 takes about 1 or 2 ms to time parameterize a path,
	  which may be an issue for real-time motion planning.
	*******************************************************************************/
	int tp_method = 1;
	bool tp_status = false;

	// ros::Time t1 = ros::Time::now();
	switch (tp_method)
	{
		case 1:
		{
			trajectory_processing::IterativeParabolicTimeParameterization iptp;
			if (iptp.computeTimeStamps(rob_traj))
				tp_status = true;
			break;
		}
		case 2:
		{
			trajectory_processing::SplineParameterization sp;
			if (sp.computeTimeStamps(rob_traj))
				tp_status = true;
			break;
		}
	}
	// ros::Time t2 = ros::Time::now();
	// ros::Duration dur = t2-t1;
	// ROS_INFO("Time parameterization took %f secs", dur.toSec());

	// collect results
	if (tp_status)
	{
		ROS_INFO("way point count is %zd", rob_traj.getWayPointCount());
		// update durations for each cfg
		cfgs_durations = rob_traj.getWayPointDurations();
		
		#ifdef DEBUG
		for (std::deque<double>::iterator it=cfgs_durations.begin(); it!=cfgs_durations.end(); ++it)
		{
			ROS_INFO("Duration from previous state is %f", *it);
		}
		#endif

		// update velocities for each cfg
		cfgs_velocites.clear();
		std::vector<double> tmp_velocites;
		for (std::size_t i=0; i<rob_traj.getWayPointCount(); ++i)
		{
			tmp_velocites.clear();
			for (int j=0; j<nDofs; ++j)
				tmp_velocites.push_back(rob_traj.getWayPoint(i).getVariableVelocity(j));
			
			#ifdef DEBUG
			std::cout << "[ ";
			for(int k=0; k<nDofs; ++k)
			{
				std::cout << tmp_velocites.at(k) << " ";
			}
			std::cout << "]" << std::endl;
			#endif

			cfgs_velocites.push_back(tmp_velocites);
		}

		if (cfgs_velocites.size() != cfgs_durations.size())
			ROS_ERROR("The number of velocity vectors is not equal to the number of durations!");
	}
	else
		ROS_ERROR("Trajectory Time Parameterization from Path Failed.");
}
// This function currently has the known issues
// 1) computeTimeIntervalsBasedOnSlowestJoint() solely based on jnt_vmax cannot guarantee to
//    satisfy the joint velocity constraints (currently by multiplying jnt_vmas by a scale of 0.25)
// 2) this function is only faster than IterativeParabolicTimeParameterization in moveit
//    with around 20 knot cfgs. it is even slower than iptp with 1000 knot cfgs due to the time consuming
//    inversion of a large matrix
void rampTrajectory::generateTrajectoryFromPathAnalytically(std::vector<double> &start_cfg_vel, std::vector<double> &end_cfg_vel)
{
	ROS_INFO("Generating trajectory %d with %zd knot cfgs from path analytically...", trajID, knot_cfgs.size());
	/*
		step 1 
		compute time stamps for knot cfgs
	*/
	// set class member cfgs_durations
	ros::Time t1 = ros::Time::now();
	computeTimeIntervalsBasedOnSlowestJoint(0.25);

	/*
		step 2
		build matrix A(h)
	*/
	int N = cfgs_durations.size(); // total number of cfgs
	int dim_A = N-2;
	Eigen::MatrixXd A = Eigen::MatrixXd::Zero(dim_A, dim_A);
	
	ros::Time t2 = ros::Time::now();
	Eigen::VectorXd s_v = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(start_cfg_vel.data(), nDofs);
	Eigen::VectorXd e_v = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(end_cfg_vel.data(), nDofs);
	ros::Time t2_1 = ros::Time::now();
	
	// fill in diagonal elements
	for (int i=0; i<dim_A; ++i)
	{
		double h_i_1 = cfgs_durations.at(i+1);
		double h_i_2 = cfgs_durations.at(i+2);
		A(i, i) = 2.0 * (h_i_1 + h_i_2);
	}

	// fill in one line above/below diagonal elements
	for (int i=0; i<(dim_A-1); ++i)
	{
		A(i,i+1) = cfgs_durations.at(i+1); // one line above
		A(i+1,i) = cfgs_durations.at(i+3); // one line below
	}
	ros::Time t2_2 = ros::Time::now();

	#ifdef DEBUG
	std::cout << "matrix A is " << std::endl;
	std::cout << A << std::endl;
	#endif

	/*
		step 3
		build matrix b(h,q,v_1, v_n)
	*/
	int dim_B = dim_A;
	Eigen::MatrixXd b = Eigen::MatrixXd::Zero(dim_B, nDofs);

	ros::Time t3 = ros::Time::now();
	Eigen::VectorXd end_v = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(end_config.data(), nDofs);
	Eigen::VectorXd start_v = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(start_config.data(), nDofs);
	ros::Time t3_1 = ros::Time::now();
	for (int i=0; i<dim_B; ++i)
	{
		double p1 = 3.0/(cfgs_durations.at(i+1) * cfgs_durations.at(i+2));
		Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i).data(), nDofs);

		Eigen::MatrixXd p2, p3, p4;
		if (i==(dim_B-1))
		{
			p2 = pow(cfgs_durations.at(i+1),2) * (end_v-knot_i);
		}
		else
		{
			Eigen::VectorXd knot_i_plus1 = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i+1).data(), nDofs);
			p2 = pow(cfgs_durations.at(i+1),2) * (knot_i_plus1-knot_i);
		}
	
		if (i==0)
		{
			p3 = pow(cfgs_durations.at(i+2),2) * (knot_i-start_v);
		}
		else 
		{
			Eigen::VectorXd knot_i_minus1 = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i-1).data(), nDofs);
			p3 = pow(cfgs_durations.at(i+2),2) * (knot_i-knot_i_minus1);
		}

		if (i==0)
			p4 = -cfgs_durations.at(i+2) * s_v;
		else if (i==(dim_B-1))
			p4 = -cfgs_durations.at(i+1) * e_v;
		else
			p4 = Eigen::MatrixXd::Zero(nDofs,1);

		b.row(i) = p1*(p2+p3).transpose()+p4.transpose();
	}
	ros::Time t3_2 = ros::Time::now();

	#ifdef DEBUG
	std::cout << "matrix b is " << std::endl;
	std::cout << b << std::endl;
	#endif

	/*
		step 4
		solve for unknown knot cfgs velocities
	*/
	ros::Time t4 = ros::Time::now();
	// Eigen::MatrixXd knot_cfg_vels = A.inverse()*b; // very slow
	Eigen::MatrixXd knot_cfg_vels = A.colPivHouseholderQr().solve(b); // moderately fast and better accuracy
	// Eigen::MatrixXd knot_cfg_vels = A.llt().solve(b); // A.llt() fastest but accuracy is low
	double relative_error_vel = (A*knot_cfg_vels-b).norm() / b.norm();
	if (relative_error_vel > 1e-6)
		ROS_ERROR("failed to solve for knot cfgs velocities.");
	else 
	{
		// update class member
		cfgs_velocites.clear();
		cfgs_velocites.push_back(start_cfg_vel);
		std::vector<double> tmp;
		tmp.resize(nDofs);
		for (int i=0; i<knot_cfg_vels.rows(); ++i)
		{
			Eigen::VectorXd::Map(&tmp[0], nDofs) = knot_cfg_vels.row(i);
			cfgs_velocites.push_back(tmp);
		}
		cfgs_velocites.push_back(end_cfg_vel);

		/*
			step 5
			collect analytical cubic polynomial equations
		*/
		ros::Time t5 = ros::Time::now();
		splines.clear();
		splines.resize(N-1);
		for (int i=0; i<splines.size(); ++i)
		{
			if (i==0)
			{
				splines.at(i).a0 = start_v;
				splines.at(i).a1 = s_v; 
			}
			else
			{
				Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i-1).data(), nDofs);
				splines.at(i).a0 = knot_i;
				splines.at(i).a1 = knot_cfg_vels.row(i-1); 
			}
			double h_k = cfgs_durations.at(i+1);
			Eigen::MatrixXd h(2,2);
			h << pow(h_k,2), pow(h_k,3), 2.0*h_k, 3.0*pow(h_k,2);

			Eigen::MatrixXd q(2,nDofs);
			if (i==0)
			{
				Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i).data(), nDofs);
				q << knot_i.transpose() - start_v.transpose() - h_k * s_v.transpose() , knot_cfg_vels.row(i) - s_v.transpose();
			}
			else if (i==(splines.size()-1))
			{
				Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i-1).data(), nDofs);
				q << end_v.transpose() - knot_i.transpose() - h_k * knot_cfg_vels.row(i-1) , e_v.transpose() - knot_cfg_vels.row(i-1);
			}
			else
			{
				Eigen::VectorXd knot_i = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i).data(), nDofs);
				Eigen::VectorXd knot_i_minus1 = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(knot_cfgs.at(i-1).data(), nDofs);
				q << knot_i.transpose() - knot_i_minus1.transpose() - h_k * knot_cfg_vels.row(i-1) , knot_cfg_vels.row(i) - knot_cfg_vels.row(i-1);
			}
			#ifdef DEBUG
			std::cout << "matrix h is " << std::endl;
			std::cout << h << std::endl;

			std::cout << "matrix q is " << std::endl;
			std::cout << q << std::endl;
			#endif

			Eigen::MatrixXd a = h.inverse()*q;
			double relative_error_a = (h*a-q).norm() / q.norm();
			if (relative_error_a < 1e-6)
			{
				splines.at(i).a2 = a.row(0);
				splines.at(i).a3 = a.row(1);
			}
			else
			{
				ROS_ERROR("failed to solve h*a=q for cubic polynomial coefficients.");
			}
		}

		// report timing
		// ros::Time t6 = ros::Time::now();
		// ros::Duration dur1 = t2-t1;
		// ros::Duration dur2 = t3-t2;
		// ros::Duration dur3 = t4-t3;
		// ros::Duration dur4 = t5-t4;
		// ros::Duration dur5 = t6-t6;
		// ros::Duration dur6 = t2_1-t2;
		// ros::Duration dur7 = t2_2-t2_1;
		// ros::Duration dur8 = t3_1-t3;
		// ros::Duration dur9 = t3_2-t3_1;
		// ROS_INFO("step %d took %f secs", 1, dur1.toSec());
		// ROS_INFO("step %d took %f secs", 2, dur2.toSec());
		// ROS_INFO("step %d took %f secs", 3, dur3.toSec());
		// ROS_INFO("step %d took %f secs", 4, dur4.toSec());
		// ROS_INFO("step %d took %f secs", 5, dur5.toSec());
		// ROS_INFO("step %d took %f secs", 21, dur6.toSec());
		// ROS_INFO("step %d took %f secs", 22, dur7.toSec());
		// ROS_INFO("step %d took %f secs", 31, dur8.toSec());
		// ROS_INFO("step %d took %f secs", 32, dur9.toSec());

		// test
		// for (int i=0; i<splines.size(); ++i)
		// {
		// 	std::cout << "spline coefficients are " << i << std::endl;
		// 	std::cout << splines.at(i).a0.transpose() << std::endl;
		// 	std::cout << splines.at(i).a1.transpose() << std::endl;
		// 	std::cout << splines.at(i).a2.transpose() << std::endl;
		// 	std::cout << splines.at(i).a3.transpose() << std::endl;
		// }
	}
}

// Note this function currently does not take into account the 
// start_cfg_vel and end_cfg_vel
// This may not respect acceleration limit ! Problem ! Caution !
void rampTrajectory::computeTimeIntervalsBasedOnSlowestJoint(double jt_vmax_scale)
{
	cfgs_durations.clear();
	Eigen::VectorXd jt_vmax = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(jnt_vel_max.data(), nDofs);
	jt_vmax = jt_vmax_scale * jt_vmax;

	cfgs_durations.push_back(0.0); // for start_config
	// 1st interval
	double h_max = 0.0; // longest interval
	double tmp_h = 0.0;
	for (int i=0; i<nDofs; ++i)
	{
		tmp_h = std::abs(knot_cfgs.front()[i] - start_config[i]) / jt_vmax[i];
		if (tmp_h > h_max)
			h_max = tmp_h;
	}
	cfgs_durations.push_back(h_max);
	
	// intermediate intervals
	for (int i=0; i<(knot_cfgs.size()-1); ++i)
	{
		h_max = 0.;
		tmp_h = 0.;
		for (int j=0; j<nDofs; ++j)
		{
			tmp_h = std::abs(knot_cfgs.at(i+1)[j] - knot_cfgs.at(i)[j]) / jt_vmax[j];
			if (tmp_h > h_max)
				h_max = tmp_h;
		}
		cfgs_durations.push_back(h_max);
	}

	// last interval
	h_max = 0.;
	tmp_h = 0.;
	for (int i=0; i<nDofs; ++i)
	{
		tmp_h = std::abs(end_config[i] - knot_cfgs.back()[i]) / jt_vmax[i];
		if (tmp_h > h_max)
			h_max = tmp_h;
	}
	cfgs_durations.push_back(h_max);

	// show results
	#ifdef DEBUG
	for (int i=0; i<cfgs_durations.size(); ++i)
	{
		ROS_INFO("duration from previous state for state %d is %f", i, cfgs_durations[i]);
	}
	#endif
}

// This function should be used after time parameterization
void rampTrajectory::convertToTrajectoryMsgs()
{
	trajectory_msgs::JointTrajectoryPoint jnt_pnt;
	float time_from_beginning = 0.0;

	m_traj_msg.joint_names = jnt_names;
	/*  execute the trajectory in 10ms from now 
		to prevent the warning of "1st point in trajectory dropped"
		10ms is empirically determined
	 */
	m_traj_msg.header.stamp = ros::Time::now()+ros::Duration(0.01);

	jnt_pnt.positions = start_config;
	// specify velocities for each joint pnt
	jnt_pnt.velocities = cfgs_velocites.front();
	time_from_beginning = cfgs_durations.front();
	jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
	m_traj_msg.points.push_back(jnt_pnt);

	for (std::size_t i=0; i<knot_cfgs.size(); ++i)
	{
		jnt_pnt.positions = knot_cfgs.at(i);
		jnt_pnt.velocities = cfgs_velocites.at(i+1);
		time_from_beginning += cfgs_durations.at(i+1);
		jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
		m_traj_msg.points.push_back(jnt_pnt);
	}

	jnt_pnt.positions = end_config;
	jnt_pnt.velocities = cfgs_velocites.back();
	time_from_beginning += cfgs_durations.back();
	jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
	m_traj_msg.points.push_back(jnt_pnt);
}

void rampTrajectory::convertToTrajectoryMsgs(trajectory_msgs::JointTrajectory &traj_msg)
{
	convertToTrajectoryMsgs();
	traj_msg = m_traj_msg;
}

void rampTrajectory::getJntCfgAtTimeT(double t, armCfg &cfg_t, std::string option)
{
	cfg_t.clear();
	// locate time interval where t belongs to
	int time_interval_ind = -1;
	double duration_in_interval = 0.0;
	double elapsed_time = 0.0;
	double t_proportion = 0.0;
	for (int i=0; i<(cfgs_durations.size()-1); ++i)
	{
		if (   t > elapsed_time 
			&& t < (elapsed_time+cfgs_durations.at(i+1)) )
		{
			time_interval_ind = i;
			duration_in_interval = t - elapsed_time;
			t_proportion = duration_in_interval / cfgs_durations.at(i+1);
			break;
		}
		elapsed_time += cfgs_durations.at(i+1);
	}

	if (option=="analytically")
	{
		if (time_interval_ind == -1)
		{
			ROS_ERROR("t is longer than trajectory duration!");
		}
		else
		{
			// printf("interval %d duration %f\n", time_interval_ind, duration_in_interval);
			const cubicPolynomial &cp_coeffs = splines.at(time_interval_ind);
			Eigen::VectorXd cfg_at_t = cp_coeffs.a0 + cp_coeffs.a1 * duration_in_interval +
									   cp_coeffs.a2 * pow(duration_in_interval,2) +
									   cp_coeffs.a3 * pow(duration_in_interval,3);
			cfg_t.resize(nDofs);
			Eigen::VectorXd::Map(&cfg_t[0], nDofs) = cfg_at_t;
		}
	}
	else if (option=="numerically")
	{
		if (time_interval_ind == -1)
		{
			ROS_ERROR("t is longer than trajectory duration!");
		}
		else
		{
			cfg_t.resize(nDofs);
			if (time_interval_ind==0) // first interval
			{
				for (int k=0; k<nDofs; ++k)
				{
					float diff = knot_cfgs.front().at(k)-start_config.at(k);
					cfg_t.at(k) = start_config.at(k) + diff*t_proportion;
				}
			}
			else if (time_interval_ind==(cfgs_durations.size()-2) ) // last interval
			{
				for (int k=0; k<nDofs; ++k)
				{
					float diff = end_config.at(k)-knot_cfgs.back().at(k);
					cfg_t.at(k) = knot_cfgs.back().at(k) + diff*t_proportion;
				}
			}
			else
			{
				for (int k=0; k<nDofs; ++k)
				{
					float diff = knot_cfgs.at(time_interval_ind).at(k)-knot_cfgs.at(time_interval_ind-1).at(k);
					cfg_t.at(k) = knot_cfgs.at(time_interval_ind-1).at(k) + diff*t_proportion;
				}
			}
		}
	}
	else
		ROS_ERROR("Unrecognized option in getJntCfgAtTimeT().");
}

bool rampTrajectory::collisionChecking()
{
	double col_time = -1.0;
	std::string col_obj_id;
	armCfg col_cfg;
	if (collisionChecking(col_time, col_obj_id, col_cfg))
		return true;
	else 
		return false;
}

// including self collision
bool rampTrajectory::collisionChecking( double &colliding_time, 
										std::string &colliding_object_id,
										armCfg &collding_cfg)
{
	double time_resolution = 0.3;
	double cur_time = 0.05;
	double traj_duration = getTrajectoryDuration();
	armCfg cur_time_cfg;
	while (cur_time < traj_duration)
	{
		getJntCfgAtTimeT(cur_time, cur_time_cfg, "analytically");
		// this may have problem when processing trajectories in parallel
		// this may be ok because isRobotColliding() gets a locked copy of planning scene
		if (ramp_setting_ptr->isRobotColliding(cur_time_cfg))
		{
			colliding_time = cur_time;
			collding_cfg = cur_time_cfg;
			ROS_INFO("Trajectory %d is colliding at time %f", trajID, colliding_time);

			// update class member
			feasibility = false;
			colliding_time_from_beginning = colliding_time;
			return true;
		}
		cur_time += time_resolution;
	}
	ROS_INFO("Trajectory %d is not colliding at any time", trajID);
	colliding_time = 0.0;
	colliding_object_id = "None";
	collding_cfg.clear();
	
	// update class member
	feasibility = true;
	colliding_time_from_beginning = colliding_time;
	return false;
}

double rampTrajectory::getTrajectoryDuration() const
{
	double dur = 0.0;
	for (int i=0; i<cfgs_durations.size(); ++i)
		dur += cfgs_durations.at(i);
	return dur;
}

double rampTrajectory::getTrajectoryKineticEnergy() const
{
	double kinetic_energy = 0.0;
	double tmp = 0.0;
	for (int i=0; i<cfgs_velocites.size(); ++i)
	{
		tmp = 0.0;
		for (int j=0; j<nDofs; ++j)
		{
			// 0.5*I*w^2
			tmp += 0.5*joints_moment_of_inertia.at(j)*pow(cfgs_velocites.at(i).at(j),2);
		}
		kinetic_energy += tmp;
	}
	return kinetic_energy;
}

double rampTrajectory::getTrajectoryAverageManipulabilityMeasure() 
{
	double mani_meas = 0.0;
	double tmp = 0.0;
	for (int i=0; i<knot_cfgs.size(); ++i)
	{
		tmp = ramp_setting_ptr->getManipulabilityIndex(knot_cfgs.at(i));
		if (tmp > 0.0)
			mani_meas += tmp;
	}
	tmp = ramp_setting_ptr->getManipulabilityIndex(end_config);
	if (tmp > 0.0)
		mani_meas += tmp;

	int total_cfg = knot_cfgs.size()+1;
	return mani_meas / double(total_cfg);
}

double rampTrajectory::getInfeasibilityCost()
{
	if (feasibility==false && colliding_time_from_beginning > 0.0)
	{
		double Q = 10000.0; // a big number
		return Q / colliding_time_from_beginning;
	}
	else
		return 0.0;
}

void rampTrajectory::evaluateTrajectory()
{
	double energy_cost = 0.0;
	double time_cost = 0.0;
	double manipulability_cost = 0.0;
	double infeasibility_cost = 0.0;
	energy_cost = cost_weights.c1 * getTrajectoryKineticEnergy() / cost_nrm_factors.a1;
	time_cost = cost_weights.c2 * getTrajectoryDuration() / cost_nrm_factors.a2;
	// note the inverse on manipulability
	manipulability_cost = cost_weights.c3 * (1.0 / getTrajectoryAverageManipulabilityMeasure()) / cost_nrm_factors.a3;
	infeasibility_cost = getInfeasibilityCost();

	cost = energy_cost + time_cost + manipulability_cost + infeasibility_cost;
	ROS_INFO("Trajectory %d has cost %f", trajID, cost);
	ROS_INFO("eneregy cost %f time cost %f manipulability %f infeasibility %f", energy_cost, time_cost, manipulability_cost, infeasibility_cost);
}	

void rampTrajectory::printTrajectoryInfo()
{
	ROS_INFO("Printing Trajectory %d Info:", trajID);
	for (int i=0; i<m_traj_msg.points.size(); ++i)
	{
		std::cout << "Configuration " << i << std::endl;
		std::cout << "Position:" << std::endl;
		std::cout << "[ ";
		for(int k=0; k<nDofs; ++k)
		{
			std::cout << m_traj_msg.points.at(i).positions.at(k) << " ";
		}
		std::cout << "]" << std::endl;

		std::cout << "Velocity:" << std::endl;
		std::cout << "[ ";
		for(int k=0; k<nDofs; ++k)
		{
			std::cout << m_traj_msg.points.at(i).velocities.at(k) << " ";
		}
		std::cout << "]" << std::endl;

		std::cout << "time_from_start:" << std::endl;
		std::cout << m_traj_msg.points.at(i).time_from_start << std::endl;
		std::cout << std::endl;
	}
}

bool rampTrajectory::generateCandidateRandomStateNumber(int amount, int& num_one, int& num_two ){
  int num = getStateCount();
  if (amount == 1)
	if (getStateCount()<=2)
	  return false;
	else{
	  std::random_device rd;  //Will be used to obtain a seed for the random number engine
	  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	  std::uniform_int_distribution<> dis(1, num - 2); // generate an int between the start and the goal
	  num_one = dis(gen);
	  num_two = -1;
	}
  else if(amount == 2 )
	if (getStateCount()<=3)
	  return false;
	else{
	  std::random_device rd;  //Will be used to obtain a seed for the random number engine
	  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	  std::uniform_int_distribution<> dis(1, num - 2); // generate an int between the start and the goal
	  num_one = dis(gen);
	  num_two = dis(gen);
	}
  else throw std::out_of_range("Asking a wrong amount of state numbers.");
  
}
/////////// Functions that involve uncertainties 

void rampTrajectory::updateStartingState(const robot_state::RobotStatePtr sample_state_ptr){

  //  (1) find the closest state to the robot state
  //  int   closestStateIndex = self.getClosestIndex(sample_state_ptr);
  int closestStateIndex = -1;
  // If the closest State checking fails
  if (closestStateIndex == -1)
	throw  std::out_of_range("Closest state index returns -1.");

  // (2) truncate the trajectory.
  // keepAfter(getState(closestStateIndex));
  
  // (3) the meory of sample_state_ptr will be copied
  //  prepend(sample_state_ptr);
}


/** 
 * \brief Calculate the signature based on the path string
 */
void rampTrajectory::updateSignature(){

  signature = signatureGenerator_.generatePathSignature(getPathString_());
	
}

std::string rampTrajectory::getPathString_() const{

  std::ostringstream oss;
  
  /// How to generate a string from a path? 
  //  printAsMatrix(oss);
  return oss.str();
  
}



/////////// Place holder functions that are to be fixed: 

bool rampTrajectory::check() const{
  // This is a place holder

  return false;
}
double rampTrajectory::costClac(){
  return 10.0;
}
