/*
 * ramp_rendering.cpp
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "ramp_rendering/ramp_rendering.h"

rampRendering::rampRendering(ros::NodeHandle &node_handle)
: fol_jnt_traj_action_client("joint_trajectory_action", true)
{
 	nh = node_handle;
 	// topic for visualizing a single joint configuration in rviz
 	jnt_cfg_topic = "/move_group/fake_controller_joint_states";
 	joint_cfg_pub = nh.advertise<sensor_msgs::JointState>(jnt_cfg_topic, 100);  
 	display_pub = nh.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 100, true);
 	// wait for action server to come up
    // while(!fol_jnt_traj_action_client.waitForServer(ros::Duration(5.0)))
    // {
    //   ROS_INFO("Waiting for the joint_trajectory_action server");
    // }
 	sleep(1.0); //sleep to set up the connection
}

// render single joint configuration in rviz
void rampRendering::renderJointConfiguration(armCfg &cfg, std::vector<std::string> jnt_names)
{
	sensor_msgs::JointState joint_state;
	joint_state.header.stamp = ros::Time::now();
	joint_state.name = jnt_names;
	joint_state.position = cfg;
	joint_cfg_pub.publish(joint_state);
	ros::spinOnce();
	std::cout << "Rendering joint configuration [ ";
	for (int i=0; i<cfg.size(); ++i)
	{
		std::cout << cfg.at(i) << " ";
	}
	std::cout << "]" << std::endl;
	ROS_INFO("Joint Configuration Rendered to Display.");
}

// In rviz, render each configuration in a joint trajectory one by one with duration 
// between configurations stopped in sleep(). One may want to interpolate
// to have a more smooth motion (show one config after another)
void rampRendering::renderJointTrajectory(rampTrajectory &ramp_traj, std::vector<std::string> jnt_names)
{
	// has to give rviz some time for display otherwise nothing is rendered
	// this may be a problem for time-sensitive trajectories
	float time_delay = 0.; 
	renderJointConfiguration(ramp_traj.start_config, jnt_names);
	sleep(ramp_traj.cfgs_durations.front()+time_delay);
	
	for (int i=0; i<ramp_traj.knot_cfgs.size(); ++i)
	{
		renderJointConfiguration(ramp_traj.knot_cfgs.at(i), jnt_names);
		sleep(ramp_traj.cfgs_durations.at(i+1)+time_delay);
	}

	renderJointConfiguration(ramp_traj.end_config, jnt_names);
	sleep(ramp_traj.cfgs_durations.back()+time_delay);
}

// render joint trajectory with interpolation (show one config after another)
void rampRendering::renderJointTrajectoryWithInterpolation(rampTrajectory &ramp_traj, std::vector<std::string> jnt_names)
{
	std::vector<armCfg> all_cfgs;
	all_cfgs.push_back(ramp_traj.start_config);
	for (int i=0; i<ramp_traj.knot_cfgs.size(); ++i)
		all_cfgs.push_back(ramp_traj.knot_cfgs.at(i));
	all_cfgs.push_back(ramp_traj.end_config);

	armCfg current_cfg, target_cfg;
	double dura = 0.0;
	int max_interpolation = 5;
	std::vector<armCfg> inter_cfgs;
	std::deque<double> inter_durations;

	for (int i=0; i<all_cfgs.size(); ++i)
	{
		if (i==0)
		{
			current_cfg = all_cfgs.at(i);
			dura = ramp_traj.cfgs_durations.front();
			inter_cfgs.push_back(current_cfg);
			inter_durations.push_back(dura);
		}
		else
		{
			target_cfg = all_cfgs.at(i);

			for(int j=1; j<=max_interpolation; ++j)
			{
				for (int k=0; k<target_cfg.size(); ++k)
				{
					float diff = target_cfg.at(k)-current_cfg.at(k);
					float ratio = float(j) / float(max_interpolation);
					current_cfg.at(k) += diff*ratio;
				}
				dura = 1.0 / float(max_interpolation) * ramp_traj.cfgs_durations.at(i);
				inter_cfgs.push_back(current_cfg);
				inter_durations.push_back(dura);
			}
		}
	}
	std::vector<armCfg>::const_iterator first = inter_cfgs.begin()+1;
	std::vector<armCfg>::const_iterator last = inter_cfgs.end()-1;
	ramp_traj.knot_cfgs = std::vector<armCfg>(first, last);
	ramp_traj.cfgs_durations = inter_durations;

	renderJointTrajectory(ramp_traj, jnt_names);
}

// render joint trajectory by sending the whole trajectory
// to /move_group/display_planned_path using displayTrajectory
// it seems this does not respect time parameterization
void rampRendering::renderJointTrajectoryMsg(rampTrajectory &ramp_traj,
							  				 std::vector<std::string> jnt_names)
{
	// prepare trajectory
	trajectory_msgs::JointTrajectoryPoint jnt_pnt;
	moveit_msgs::RobotTrajectory traj_msg;
	float time_from_beginning = 0.0;

	traj_msg.joint_trajectory.joint_names = jnt_names;

	jnt_pnt.positions = ramp_traj.start_config;
	time_from_beginning = ramp_traj.cfgs_durations.front();
	jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
	traj_msg.joint_trajectory.points.push_back(jnt_pnt);

	for (std::size_t i=0; i<ramp_traj.knot_cfgs.size(); ++i)
	{
		jnt_pnt.positions = ramp_traj.knot_cfgs.at(i);
		time_from_beginning += ramp_traj.cfgs_durations.at(i+1);
		jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
		traj_msg.joint_trajectory.points.push_back(jnt_pnt);
	}

	jnt_pnt.positions = ramp_traj.end_config;
	time_from_beginning += ramp_traj.cfgs_durations.back();
	jnt_pnt.time_from_start = ros::Duration(time_from_beginning);
	traj_msg.joint_trajectory.points.push_back(jnt_pnt);

	// prepare start state
	moveit_msgs::RobotState traj_start;
	traj_start.joint_state.name = jnt_names;
	traj_start.joint_state.position = ramp_traj.start_config;

	// make msg
	moveit_msgs::DisplayTrajectory display_trajectory;
	display_trajectory.trajectory.push_back(traj_msg);
	display_trajectory.trajectory_start = traj_start;

	display_pub.publish(display_trajectory);
}

// using follow_joint_trajectory. The trajectory is executed
// in gazebo with controllers and rviz also displays the robot state
void rampRendering::renderJointTrajectoryGazebo(rampTrajectory &ramp_traj)
{
	// convert to trajectory msg
	trajectory_msgs::JointTrajectory traj_msg;
	ramp_traj.convertToTrajectoryMsgs(traj_msg);
	ramp_traj.printTrajectoryInfo();
	
	// make control msgs
	control_msgs::FollowJointTrajectoryGoal traj_goal;
	traj_goal.trajectory = traj_msg;
	fol_jnt_traj_action_client.sendGoal(traj_goal);
	
	// Wait for trajectory completion
	// Caution! This is not correct
	// cannot rely on this for checking trajectory execution complete
	// It also seems that this correctly detects the completion of execution
	// More thorough tests needed
	while(!fol_jnt_traj_action_client.getState().isDone() && ros::ok())
	{
		usleep(50000);
	}
	ROS_INFO("The trajectory has been executed");
}

rampRendering::~rampRendering()
{

}
		



