/*
 * ramp_trajectory.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990> 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_TRAJECTORY_H
#define RAMP_TRAJECTORY_H
 
#include <ros/ros.h>

#include "ramp_setting/ramp_setting.h"
#include "ramp_core/ramp_common.h"
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>
#include <moveit/trajectory_processing/spline_parameterization.h>

#include <random>
#include <boost/shared_ptr.hpp>
#include <stdexcept> // This header includes <exception>
#include <string>
#include "ramp_population/pathSignature.h"

// solve Ax=b
#include <Eigen/Cholesky>
#include "ramp_util/ramp_util.h"
#include <chrono>

// cubic polynomial for joint vector
struct cubicPolynomial
{
	Eigen::VectorXd a0, a1, a2, a3;
};

struct costFunctionWeights
{
	double c1, c2, c3;
};

struct costFunctionNormalizationFactor
{
	double a1, a2, a3;
};

class rampTrajectory
{
	public:
		rampTrajectory(boost::shared_ptr<rampSetting> &r_set_p);

		/** 
		 * \brief Set basic infomation of a trajectory
		 * \param start and end configurations and trajectory ID
		 * \return void
		 */
		void setTrajectory(armCfg &start_cfg, armCfg &end_cfg, int trajectoryID);

		void getJntNames(std::vector<std::string> &jnt_names);

		/** 
		 * \return number of Dofs
		 */
		int getNumOfDofs() const;

		/** 
		 * \return trajectory ID
		 */
		int getTrajectoryID() const;

		/** 
		 * \brief Generate trajectory from path (add time stamps and compute velocites)
		 *        This uses the trejectory generator from MoveIt.
		 *        This uses IterativeParabolicTimeParameterization to generate a time-optimal trajectory. 
		 *        One can also use spline functions (MoveIt provided but not incorporated here.)
		 *        It fills information for cfgs_durations and cfgs_velocities
		 * \param 
		 * \return void
		 */
		void generateTrajectoryFromPathNumerically(const std::vector<double> &start_cfg_vel);
		
		/** 
		 * \brief Generate trajectory from path (add time stamps and compute velocites)
		 *        This analytically constructs cubic polynomials.
		 *        It fills information for cfgs_durations and cfgs_velocities
		 * \param start_cfg velocity and end_cfg velocity
		 * \return void
		 */
		void generateTrajectoryFromPathAnalytically(std::vector<double> &start_cfg_vel, std::vector<double> &end_cfg_vel);

		/** 
		 * \brief Get joint cfg at time t
		 * \param time t, armCfg to be filled in, option = "analytically" or "numerically"
		 * \return void
		 */
		void getJntCfgAtTimeT(double t, armCfg &cfg_t, std::string option);

		/** 
		 * \brief trajectory total duration
		 */
		double getTrajectoryDuration() const;

		/** 
		 * \brief trajectory total kinetic energy
		 */
		double getTrajectoryKineticEnergy() const;

		/** 
		 * \brief trajectory average manipulability measure
		 */
		double getTrajectoryAverageManipulabilityMeasure();

		/** 
		 * \brief get infeasibility cost if this trajectory is infeasible
		 		  should be called after collisionChecking() of this trajectory
		 */
		double getInfeasibilityCost();

		/** 
		 * \brief trajectory collision checking with interpolation
		 */
		bool collisionChecking();
		bool collisionChecking(double &colliding_time, std::string &colliding_object_id, armCfg &collding_cfg);

		/** 
		 * \brief Converts information inside this class to a trajectory_msgs
		 *        to facilitate the use of MoveIt existing functions.
		 * \param trajectory_msgs to be filled in (It also fills in information
		 *        for m_traj_msg)
		 * \return void
		 */
		void convertToTrajectoryMsgs();
		void convertToTrajectoryMsgs(trajectory_msgs::JointTrajectory &traj_msg);
		
		/** 
		 * \brief Evaluate a trajectory (to be implemented).
		 * \param 
		 * \return void
		 */
		void evaluateTrajectory();

		/** 
		 * \brief Compute diversity measure
		 * \param 
		 * \return void
		 */
		void computeDiversityMeasure();

		/** 
		 * \brief Print trajectory information contained in m_traj_msg.
		 *        One needs to make sure the information is up-to-date.
		 *        Or this should only be called after convertToTrajectoryMsgs()
		 *        Positions, velocities and time stamps.
		 * \return void
		 */
		void printTrajectoryInfo();

		/** 
		 * \brief Update the starting position of a path, discard the outdated part. 
		 * \param sample_state denotes the new robot state 
		 * \return void
		 * This is necessary when the robot moves along the planned path
		 */
		void updateStartingState(const robot_state::RobotStatePtr sample_state_ptr);

		/** 
		 * \brief Choose one or two states for the operators to work on 
		 * \param amount one or two states
		 * \param num_one takes the generate number one
		 * \param num_two
		 * \return fail in case that there are too few states.
		 */
		bool generateCandidateRandomStateNumber(int amount, int& num_one, int& num_two );

		/** 
		 * \return number of the states in a trajectory 
		 **/
		int getStateCount() const {
		  return knot_cfgs.size();
		}

		// We use the following public attributes to support the boost::multi_index_container.

		/// Overload the '<' operator such that we can use the boost::multi_index::identity
		bool operator<(const rampTrajectory & e)const{return cost<e.cost;}

		/** 
		 * \brief Calculate the cost of a path w.r.t an objective
		 * \return the cost in double 
		 */
		double costClac();

		/// It is used as an id for hashing
		int signature;
		/** 
		 * \brief Calculate the signature based on the trajectory string
		 */
		void updateSignature();

		/* 
		 * \brief update the feasibility of the trajectory 
		 * \return void
		 */
		void updateFeasibility(){
		  feasibility = check();
		}

		/* 
		 * \brief trajectory cost after evaluateTrajectory()
	 			  We use the 'cost' to rank  
		 */
		double cost;
		
		/* 
		 * \brief Feasibility of the path, we use it together with 
		 		  the cost to construct a composite key 
		 */
		bool feasibility;

		/* 
		 * \brief colliding time from beginning of trajectory if it is
		 		  infeasible
		 */
		double colliding_time_from_beginning;

		/* 
		 * \brief trajectory start and end configurations
		 */
		armCfg start_config, end_config;

		/* 
		 * \brief trajectory knot configurations
		 */
		std::vector<armCfg> knot_cfgs;

		/* 
		 * \brief duration for each configuration in this trajectory 
		 *        (time from the previous configuration)
		 *        the starting configuration has zero
		 */
		std::deque<double> cfgs_durations; //Duration from previous state (including start_config + knot configs + end_config)
		
		/* 
		 * \brief velocites for each configuration in this trajectory 
		 */
		std::vector<std::vector<double> > cfgs_velocites;

	private:
		/* 
		 * \brief number of Dofs
		 */
		int nDofs;

		/* 
		 * \brief trajectory ID
		 */
		int trajID;

		/* 
		 * \brief joint names
		 */
		std::vector<std::string> jnt_names;

		/* 
		 * \brief joint velocity max
		 */
		std::vector<double> jnt_vel_max;

		boost::shared_ptr<rampSetting> ramp_setting_ptr;

		/* 
		 * \brief trajectory information in the form of trajectory_msgs
		 */
		trajectory_msgs::JointTrajectory m_traj_msg;
		int n_config;

		/* 
		 * \brief cubic polynomials coefficients
		 */
		std::vector<cubicPolynomial> splines;

		/* 
		 * \brief cost function weights
		 */
		costFunctionWeights cost_weights;

		/* 
		 * \brief moment of inertia for each joint
		 */
		std::vector<double> joints_moment_of_inertia;

		/* 
		 * \brief cost function normalization factor
		 */
		costFunctionNormalizationFactor cost_nrm_factors;

		/* 
		 * \brief compute time intervals between two cfgs based on the 
		 		  velocity of the slowest joint.
		 		  This is to be used next for generating trajectory 
		 		  analytically using cubic polynomials
		 */
		void computeTimeIntervalsBasedOnSlowestJoint(double jt_vmax_scale=1.0);
		
		/* 
		 * \brief check the feasibility of the trajectory
		 * \return void
		 */
		bool check() const;

			
		pathSignature signatureGenerator_;
		/** 
		 * \brief Get the string of a path
		 * \return a string that includes all the joint values of all the states.
		 */
		std::string getPathString_() const;
};

typedef boost::shared_ptr<rampTrajectory> rampTrajectoryPtr;

#endif
