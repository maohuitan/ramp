/*
 * ramp_sensing.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_SENSING_H
#define RAMP_SENSING_H

#include "ramp_obstacle/ramp_obstacle.h"
#include "ramp_core/ramp_common.h"
#include "gazebo_msgs/ModelStates.h"

class rampSensing
{
	public:
		rampSensing(ros::NodeHandle &nh);
		void addObstacle(rampObstacle &rob);
		void removeObstacleByID(const std::string &id);
		void callback(const gazebo_msgs::ModelStates::ConstPtr& msg);
		void update();

	private:
		std::vector<rampObstacle> obstacles;
		ros::Subscriber sub;
		std::string sensing_topic;
		int num_of_sensing_cyc;
};


#endif
