/*
 * ramp_core.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_CORE_H
#define RAMP_CORE_H

#include <ros/ros.h>

#include "ramp_core/ramp_common.h"
#include "ramp_population/ramp_population.h"
#include "ramp_setting/ramp_setting.h"
#include "ramp_goal/ramp_goal.h"
#include "ramp_trajectory/ramp_trajectory.h"
#include "ramp_operators/ramp_operators.h"
#include "ramp_rendering/ramp_rendering.h"
#include "ramp_sensing/ramp_sensing.h"
#include "ramp_test/ramp_test.h"

#include <moveit_msgs/ApplyPlanningScene.h>
#include <sensor_msgs/JointState.h>

struct statistics
{
	int num_of_ctrl_cyc;
	int num_of_plan_cyc;
};

class rampCore
{
	public:
		rampCore();
		void initialize();
		void run();
		void showPopulation();
		void robotStateCallback(const sensor_msgs::JointState::ConstPtr& msg);

	private:
		ros::NodeHandle nh;
		armCfg robot_cfg;
		boost::shared_ptr<rampSetting> ramp_setting_ptr;
		boost::shared_ptr<rampSampling> ramp_sampling_ptr;
		boost::shared_ptr<rampPopulation> ramp_population_ptr;
		rampGoal ramp_goal;
		boost::shared_ptr<rampSensing> ramp_sensing_ptr;
		boost::shared_ptr<rampRendering> ramp_rendering_ptr;
		boost::shared_ptr<rampOperators> ramp_oprt_ptr;

		ros::Subscriber robot_state_sub;
		std::string robot_state_topic;
		statistics stats;

		void do_control();
		void do_planning();
};


#endif
