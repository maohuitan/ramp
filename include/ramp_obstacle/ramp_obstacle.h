/*
 * ramp_obstacle.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_OBSTACLE_H
#define RAMP_OBSTACLE_H

#include "ramp_core/ramp_common.h"
#include <moveit_msgs/ApplyPlanningScene.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/PlanningScene.h>

struct boundingSphere
{
	Eigen::Vector3d center; // box center
	double radius;
};

class rampObstacle
{
	public:
		rampObstacle(std::string &obs_type, std::string &obs_id,
					 const geometry_msgs::Pose &obs_pose, const Eigen::Vector3f &dimension,
					 boost::shared_ptr<ros::ServiceClient> &psdc_ptr);
		~rampObstacle();
		void setNewPose(const geometry_msgs::Pose &pose);
		void setNewVelocity(const Eigen::VectorXf &vel);
		std::string getID();
		void clear();

	private:
		std::string id;
		std::string type;
		geometry_msgs::Pose pose; 
		Eigen::VectorXf velocity; // XYZRPY velocity
		Eigen::Vector3f dimens;
		boost::shared_ptr<ros::ServiceClient> planning_scene_diff_client_ptr;

		moveit_msgs::AttachedCollisionObject attached_object;
		shape_msgs::SolidPrimitive primitive;
		void addToPlanningScene();
		void publishObstacleDiff();
};


#endif
