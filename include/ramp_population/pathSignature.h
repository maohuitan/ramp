# ifndef PATHSIGNATURE_H
# define PATHSIGNATURE_H

/* # include <rampPath/rampPath.h> */
/* # include <ompl/geometric/PathGeometric.h> */
# include <boost/shared_ptr.hpp>
# include <boost/functional/hash.hpp>

class pathSignature{

  /* 
   * \brief Based on the path string, we use a hash function to generate an unique hash key for a given rampPath.
   */
 private:

  /* boost::shared_ptr<rampPath> hostPathPtr_; */
  boost::hash<std::string> stringHash_;
    
 public:
  /** \brief Constructor
   * \param We register a pathSignature generator to a certain ramp path
  */
  /* pathSignature(rampPath* host_path){ */
  /*   hostPathPtr_ = host_path; */
  /* } */
  pathSignature(){
  }

  /** \brief Destructor
  */
  ~pathSignature(){

  }

  /** \brief Generate a signature for a path
   *  \param input_path_string that is generated from a rampPath. 
   *  \return a hash key
   *
   * We may simply use the 'print' member function to generate a string. 
  */
  std::size_t  generatePathSignature(const std::string& input_path_string) const;

  
};// end of stateSignature class

# endif
