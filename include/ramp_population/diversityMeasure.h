
# ifndef DIVERSITYMEASURE_H
# define DIVERSITYMEASURE_H

# include "ramp_trajectory/ramp_trajectory.h"

class diversityMeasure{
/** 
 * \brief Calculate the diversity measure for a path. We use this measure to decide how to assign the path the corresponding group. 
 */
 public:
  diversityMeasure(){
    
  }
  ~diversityMeasure(){
    // free memo
  }
  
  /* double pathMeasureCalc(const rampPath* new_path); */
  double trajectoryMeasureCalc(const rampTrajectory* new_trajectory);
    
 private:


  
};


# endif
