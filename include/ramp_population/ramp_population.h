/*
 * ramp_population.h
 * 
 * Copyright 2017 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef RAMP_POPULATION_H
#define RAMP_POPULATION_H


# include "ramp_trajectory/ramp_trajectory.h" 
# include "ramp_population/ramp_group.h" 
# include "ramp_population/diversityMeasure.h"
#include "ramp_operators/ramp_operators.h"

# include <boost/multi_index_container.hpp>
# include <boost/multi_index/hashed_index.hpp>
# include <boost/multi_index/ordered_index.hpp>
# include <boost/multi_index/identity.hpp>
# include <boost/multi_index/member.hpp>
# include <boost/multi_index/tag.hpp>
// random_access allows you to treat the MultiIndex container like a vector
#include <boost/multi_index/random_access_index.hpp>

struct tag_random {};
struct tag_best{};
struct tag_worst {};

# include <stdexcept>
# include <limits>

typedef boost::multi_index::multi_index_container<
boost::shared_ptr<rampTrajectoryGroup>,
  boost::multi_index::indexed_by<
  // (0) sort on the cost of the best path 
  boost::multi_index::ordered_non_unique<
  boost::multi_index::tag<tag_best>, boost::multi_index::member<rampTrajectoryGroup, double, &rampTrajectoryGroup::bestCost>
  >,
// (1) sort on the cost of the worst path 
  boost::multi_index::ordered_non_unique<
  boost::multi_index::tag<tag_worst>, boost::multi_index::member<rampTrajectoryGroup, double, &rampTrajectoryGroup::worstCost>
  >,
// (2) access to an element with position
  boost::multi_index::random_access<
  boost::multi_index::tag<tag_random>
  >
  >// end of indexed_by
  > trajectoryGroupContrainer;



class rampPopulation{
  /** 
   * \brief 
   *
   * A population include at least one group. 
   * (1) We can start with a fixed number of groups. 
   * (2) we should be able to infer the necessary amount of groups. We could extract information from the diversity measure, the Euclidean space inforamtion, the configuration space information and the initial set of trajectories/path ...   
   */

 public:
  /** 
   * \brief Register the path population to a certain planner
   * \param planner_ptr
   * \param group_amount Suppose we start with 10 groups
   */
  /* rampPopulation(rampPlanner * planner_ptr, int group_amount =  10){ */
  rampPopulation( int group_amount = 10){
    num_of_trajs = 3;
    groupAmount_ = group_amount;
    /* rampPlannerPtr_  = planner_ptr; */
    diversityMeasureCalculatorPtr_.reset(new diversityMeasure() );
  }
  ~rampPopulation(){ };

  /* HM - Is this necessary?
   * \brief In the beginning of each control period, we need to re-define the subgroups. 
   * \return success status
   */
  /// This function is not yet defined 
  bool regroupPathPopulation();
  
  /** HM - Is this necessary?
   * \brief Check if a trajectory is unique in this group.
   * \param new_trajectory is the trajectory to be checked. 
   * \param return  
   */
  bool isUniqueTrajectory(const boost::shared_ptr<rampTrajectory> new_trajectory_ptr) const;


  /* 
   * \brief In the beginning of each control period, we need to update the first state of all the path and discard the outdated part.
   * If there are too less states. This operation may lead to identical trajectory. Let's keep an eye on it
   * \return success status
   */
  bool updateStartingState(const robot_state::RobotStatePtr sample_state_ptr);



  /** 
   * \brief Evaluate the trajectory population and calculate the best trajectory, which has to be feasible
   * 1. update the best feasible/infeasible trajectory in each subgroup
   * 2. update the subgroup best trajectory    HM - Is this necessary?
   * 3. update the subgroup worst trajectory   HM - Is this necessary?
   * \return bestTrajectoryPtr 
   */
  boost::shared_ptr<rampTrajectory> calcBestTrajectory();


  /** HM - Why do we want to replace the best trajectory?
   * \brief Replace a trajectory in the population, this trajectory may or may not be feasible. 
   * \param newTrajectoryPtr pointing to the new path 
   * \return bestTrajectoryPtr
   */
  void replaceBestTrajectory(rampTrajectory* newTrajectoryPtr){
    bestTrajectoryPtr.reset( newTrajectoryPtr);
    bestCost = newTrajectoryPtr->cost;
    /* bestCost = bestTrajectoryPtr->cost( */
    /* 				       rampPlannerPtr_.environmentSetup_.getProblemDefinition().getOptimizationObjective() */
    /* 				       ).value(); */
  }




  /** 
   * \brief Check if the best path in the group has been updated
   * \return true if there was a change
   */
  bool updateBestTrajectory();

  /** 
   * \brief Check if the worst path in the group has been updated
   * \return true if there was a change
   */
  bool updateWorstTrajectory();

  /// pointer to the best trajectory
  boost::shared_ptr<rampTrajectory> bestTrajectoryPtr;
  /// best cost
  double bestCost;
  /// pointer to the worst trajectory
  boost::shared_ptr<rampTrajectory> worstTrajectoryPtr;
  /// worst cost
  double worstCost;





  /* HM */
  void buildPopulation(boost::shared_ptr<rampSetting> &r_set_ptr);
  void initializePopulation(boost::shared_ptr<rampOperators> &r_oprt_ptr);
  void evaluateTrajectoriesInPopulation();
  void printPopulationInfo();


 private:
  /// Functor to update the path group best cost 

  struct update_group_best_cost_{
    void operator()(boost::shared_ptr<rampTrajectoryGroup> g)
    {
      g->updateBestTrajectory();
    }
  };


  /// Functor to update the path group worst cost 
  struct update_group_worst_cost_{
    void operator()(boost::shared_ptr<rampTrajectoryGroup> g)
    {
      g->updateWorstTrajectory();
    }
  };

  /** 
   * \brief Calculates the group number of a certain Path
   * \param newPathPtr
   * \return the group number.
   */
  int calcTrajectorySubGroupNumber_(const rampTrajectory* newTrajectoryPtr) const;

  /// Number of groups
  int groupAmount_;


  /// pointer to the ramp planner that hosts this path population
  /* boost::shared_ptr<rampPlanner> rampPlannerPtr_; */

  /// A container of pointer that are pointing to the groups
  //  std::vector<boost::shared_ptr<rampPathGroup> > groupContainer_;
  trajectoryGroupContrainer groupContainer_;
  /// Objects that calculates a diversity measure of a path
  boost::shared_ptr<diversityMeasure> diversityMeasureCalculatorPtr_;




  /* HM */
  int num_of_trajs;
  std::vector<rampTrajectoryPtr> trajectories;

};


#endif
