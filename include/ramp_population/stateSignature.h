# ifndef STATESIGNATURE_H
# define STATESIGNATURE_H

# include <ompl/base/StateSpace.h>
# include <iostream>
# include <string>
# include <boost/scoped_ptr.hpp>
# include <boost/functional/hash.hpp>

class stateSignature{
 private:
  
  boost::hash<std::string> stringHash_;
    
 public:
  /** \brief Constructor
  */
  stateSignature(){
  }
  /** \brief Destructor
  */
  ~stateSignature(){

  }

  /** \brief Generate a signature for a state
   *  \param Reference to the input state
   *  \return a copy of 'the string signature'
  */
  std::size_t  generateStateSignature(const std::string& input_state_string) const;

  
};// end of stateSignature class

# endif
