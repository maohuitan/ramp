/*
 * ramp_population.h
 * 
 * Copyright 2017 yuquan <yuquan@kth.se>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */



# ifndef RAMP_GROUP_H
# define RAMP_GROUP_H

# include "ramp_trajectory/ramp_trajectory.h"
# include <boost/shared_ptr.hpp>

# include <boost/multi_index_container.hpp>
# include <boost/multi_index/composite_key.hpp>
# include <boost/multi_index/hashed_index.hpp>
# include <boost/multi_index/ordered_index.hpp>
# include <boost/multi_index/sequenced_index.hpp>
# include <boost/multi_index/identity.hpp>
# include <boost/multi_index/member.hpp>
# include <boost/multi_index/tag.hpp>
// random_access allows you to treat the MultiIndex container like a vector
#include <boost/multi_index/random_access_index.hpp>

struct tag_cost {};
struct tag_id {};
struct tag_as_inserted {};
struct tag_composite {};
struct tag_feasibility {};


typedef boost::multi_index::multi_index_container<
boost::shared_ptr<rampTrajectory>,
  boost::multi_index::indexed_by<
  // (0) sorted by operator on non-unique cost(we use non_unique cost)
  // might be redundant
  boost::multi_index::ordered_non_unique<
  boost::multi_index::tag<tag_cost>, 
  boost::multi_index::identity<rampTrajectory>      
  >,
// (1) hashed on the path identity 
  boost::multi_index::hashed_unique<
  boost::multi_index::tag<tag_id>, 
  boost::multi_index::member<rampTrajectory, int, &rampTrajectory::signature>
  >,
// (2) access to an element with position
  boost::multi_index::random_access<
  >,
  // (3) composite key:
  boost::multi_index::ordered_non_unique<
  boost::multi_index::tag<tag_composite>, boost::multi_index::composite_key<
  rampTrajectory,
  boost::multi_index::member<rampTrajectory, bool, &rampTrajectory::feasibility>,
  boost::multi_index::identity<rampTrajectory>
  >// end of composite key
  >,// end of ordered_non_unique
// (4) feasibility key
  boost::multi_index::hashed_non_unique<
  boost::multi_index::tag<tag_feasibility>, boost::multi_index::member<rampTrajectory, bool, &rampTrajectory::feasibility>
  >
  >// end of indexed by
  > rampTrajectorySet;


class rampTrajectoryGroup{
  /** 
   * \brief 
   *
   * A population include at least one group. 
   */
 public:

  rampTrajectoryGroup(int size_limit){
    sizeLimit_ = size_limit;
    

  }
  virtual ~rampTrajectoryGroup(){
  }

  /* /// Overload the '<' operator such that we can use the boost::multi_index::identity */
  /* bool operator<(const rampPathGroup& e)const{return cost<e.cost;} */
    /// Points to the best Path in the group 
  boost::shared_ptr<rampTrajectory> bestTrajectoryPtr;
  boost::shared_ptr<rampTrajectory> worstTrajectoryPtr;
  double bestCost;
  double worstCost;
  
  bool isUniqueTrajectory(const boost::shared_ptr<rampTrajectory> new_trajectory_ptr) const{

    const rampTrajectorySet::index<tag_id>::type & id_index = trajectoryContainer_.get<tag_id>();
    if(id_index.find(new_trajectory_ptr->signature)==id_index.end()){
      // did not find it in the group
      return true;
    }else{
      // Found it in the group}
      return false;
    }
  }


  
  /** 
   * \brief Take a new path into the group.  
   * \param new_path
   * \return the operation status 
   */
  bool takeTrajectory(rampTrajectory* new_trajectory){

    if(trajectoryContainer_.size() == sizeLimit_)
      return false;
    
    new_trajectory_ptr_.reset(new_trajectory);
    return trajectoryContainer_.insert(new_trajectory_ptr_).second;
  }


  /* read  the number of path */
  int readSize(){
    return  trajectoryContainer_.size();
  }
  

  
  void updateTrajectoryContainer(
				 );

  void updateOneTrajectory(const boost::shared_ptr<rampTrajectory> p 
			   );


  /** 
   * \brief Check if the best trajectory in the group has been updated
   * \return true if there was a change
   */
  bool updateBestTrajectory();

  /** 
   * \brief Check if the worst trajectory in the group has been updated
   * \return true if there was a change
   */
  bool updateWorstTrajectory();
  
  /** 
   * \brief Check if the best infeasible trajectory has changed 
   * \return true if there was a change
   */
  bool updateBestInfeasible();

  
  /** 
   * \brief Check if the best feasible trajectory has changed
   * \return true if there was a change
   */
  bool updateBestFeasible();

  int getInfeasibleTrajectoryNumber() const{
    const rampTrajectorySet::index<tag_feasibility>::type & 
      infeasibility_index = trajectoryContainer_.get<tag_feasibility>();
    return infeasibility_index.count(false);
  }

  
  
  int getFeasibleTrajectoryNumber() const{
    const rampTrajectorySet::index<tag_feasibility>::type & 
      feasibility_index = trajectoryContainer_.get<tag_feasibility>();
    return feasibility_index.count(true);
  }


  /* 
   * \brief Update the starting state of all the path 
   * \param  robot_state::RobotStatePtr sample_state_ptr points to the new starting state
   */
  void updateStartingState(const robot_state::RobotStatePtr sample_state_ptr);
  

  
 private:

  /// Functor to update the path cost 
  struct update_trajectory_cost_{
    void operator()(boost::shared_ptr<rampTrajectory> p)
    {
      p->costClac();
    }
  };

  /* struct update_path_cost_{ */
  /* update_path_cost_( */
  /* 		    const base::OptimizationObjectivePtr * obj_ptr */
  /* 		    ) : obj_ptr_(obj_ptr) {}     */
  /*   void operator()(boost::shared_ptr<rampPath> p) */
  /*   { */
  /*     p->costClac(*obj_ptr_); */
  /*   } */
  /* private: */
  /*   const base::OptimizationObjectivePtr * obj_ptr_; */
  /* }; */

  /// Functor to update the path  feasibility
  struct update_trajectory_feasibility_{
    void operator()(boost::shared_ptr<rampTrajectory> p)
    {
      p->updateFeasibility();
    }
  
  };

  
  /// Functor to update the path  id
  struct update_trajectory_id_{
    void operator()(boost::shared_ptr<rampTrajectory> p)
    {
      p->updateSignature();
    }
  };


  
  
  
  /// Group size limit
  int sizeLimit_;
  
  /// Hash table that keeps the infeasible path
  
  boost::shared_ptr<rampTrajectory> bestFeasibleTrajectoryPtr_;
  boost::shared_ptr<rampTrajectory> bestInfeasibleTrajectoryPtr_;

  boost::shared_ptr<rampTrajectory> worstFeasibleTrajectoryPtr_;
  boost::shared_ptr<rampTrajectory> worstInfeasibleTrajectoryPtr_;
  rampTrajectorySet trajectoryContainer_;
  boost::shared_ptr<rampTrajectory> new_trajectory_ptr_;
  
};
# endif 
